<?php 
use app\models\MedicalCardForm;
$this->title = Yii::t('patient_medical_form', 'Medical card №') . $model->medical_card_number;
?>
<HEAD>
<STYLE TYPE="text/css">
	<!--
		@page { margin-left: 0.79in; margin-right: 0.79in; margin-top: 0.59in; margin-bottom: 0.59in }
		P { margin-bottom: 0in; direction: ltr; color: #000000; text-align: center; widows: 2; orphans: 2 }
		P.western { font-family: "Times New Roman", serif; font-size: 10pt; so-language: uk-UA }
		P.cjk { font-family: "Times New Roman", serif; font-size: 10pt }
		P.ctl { font-family: "Times New Roman", serif; font-size: 10pt; so-language: ar-SA }
		H1 { margin-top: 0in; margin-bottom: 0in; direction: ltr; color: #000000; text-align: center; widows: 2; orphans: 2 }
		H1.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: uk-UA; font-weight: normal }
		H1.cjk { font-family: "Times New Roman", serif; font-size: 12pt; font-weight: normal }
		H1.ctl { font-family: "Times New Roman", serif; font-size: 10pt; so-language: ar-SA; font-weight: normal }
		H2 { margin-top: 0in; margin-bottom: 0in; direction: ltr; color: #000000; text-align: center; widows: 2; orphans: 2 }
		H2.western { font-size: 10pt; so-language: uk-UA }
		H2.cjk { font-family: "Times New Roman", serif; font-size: 10pt }
		H2.ctl { font-family: "Times New Roman", serif; font-size: 10pt; so-language: ar-SA; font-weight: normal }
		H3 { margin-top: 0in; margin-bottom: 0in; direction: ltr; color: #000000; text-align: center; widows: 2; orphans: 2 }
		H3.western { font-size: 12pt; so-language: uk-UA }
		H3.cjk { font-family: "Times New Roman", serif; font-size: 12pt }
		H3.ctl { font-family: "Times New Roman", serif; font-size: 10pt; so-language: ar-SA; font-weight: normal }
	-->
	</STYLE>
</HEAD>
<BODY>
	<TABLE ALIGN=RIGHT WIDTH=257 CELLPADDING=7 CELLSPACING=0  style="border: 1px solid black; page-break-before: always;">
		<COL WIDTH=239>
		<TR>
			<TD WIDTH=239 VALIGN=TOP STYLE="border: 1.50pt solid #000000; padding: 0in 0.08in">
				<H1 LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2>Код
				форми за ЗКУД</FONT></H1>
				<H1 LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2>Код
				закладу за ЗКПО</FONT></H1>
			</TD>
		</TR>
	</TABLE>
	<BR>
	<TABLE WIDTH=702 CELLPADDING=7 CELLSPACING=0>
		<COL WIDTH=115>
		<COL WIDTH=4356>
		<COL WIDTH=30>
		<COL WIDTH=18>
		<COL WIDTH=14>
		<COL WIDTH=24>
		<COL WIDTH=24>
		<COL WIDTH=24>
		<COL WIDTH=19>
		<COL WIDTH=10>
		<COL WIDTH=52>
		<COL WIDTH=213>
		<TBODY>
			<TR VALIGN=TOP>
				<TD ALIGN=CENTER COLSPAN=6 WIDTH=258 STYLE="border-top: 1.50pt solid #000000; border-bottom: none; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><FONT SIZE=2 STYLE="font-size: 9pt">Міністерство
					охорони здоров’я України </FONT>
					</P>
					<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><FONT SIZE=2 STYLE="font-size: 9pt">Найменування
					закладу</FONT></P>
				</TD>
				<TD COLSPAN=3 WIDTH=94 STYLE="border-top: 1.50pt solid #000000; border-bottom: none; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><BR>
					</P>
				</TD>
				<TD COLSPAN=3 WIDTH=303 STYLE="border-top: 1.50pt solid #000000; border-bottom: none; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">МЕДИЧНА
					ДОКУМЕНТАЦІЯ </FONT>
					</P>
					<H2 LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">ФОРМА
					№ 003/о</FONT></H2>
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Затверджена
					наказом МОЗ України</FONT></P>
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">260799
					р.    № 184</FONT></P>
				</TD>
			</TR>
		</TBODY>
		<TBODY>
			<TR>
				<TD COLSPAN=12 WIDTH=684 VALIGN=TOP STYLE="border-top: 1.50pt solid #000000; border-bottom: none; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in" ALIGN=CENTER>
					<P LANG="uk-UA" CLASS="western" ALIGN=CENTER>       <FONT SIZE=2 STYLE="font-size: 9pt"><B>МЕДИЧНА
					КАРТА № <SPAN style='text-decoration: underline;'><?= $model->medical_card_number; ?></SPAN></B></FONT></P>
					<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><BR>
					</P>
					<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><FONT SIZE=2 STYLE="font-size: 9pt"><B>СТАЦІОНАРНОГО
					ХВОРОГО </B></FONT>
					</P>
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
			</TR>
		</TBODY>
		<TBODY>
			<TR VALIGN=TOP>
				<TD WIDTH=115 HEIGHT=8 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="ru-RU" CLASS="western" ALIGN=LEFT><BR>
					</P>
					<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><FONT SIZE=2 STYLE="font-size: 9pt">Госпіталізація</FONT></P>
					<P LANG="ru-RU" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD COLSPAN=2 WIDTH=31 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><?= date('d',strtotime($model->hospitalisation_datetime));?><BR>
					</P>
				</TD>
				<TD WIDTH=18 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><?= date('m',strtotime($model->hospitalisation_datetime));?><BR>
					</P>
				</TD>
				<TD WIDTH=14 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><?= date('Y',strtotime($model->hospitalisation_datetime));?><BR>
					</P>
				</TD>
				<TD WIDTH=24 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=24 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=24 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD COLSPAN=2 WIDTH=43 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><?= date('H',strtotime($model->hospitalisation_datetime));?><BR>
					</P>
				</TD>
				<TD WIDTH=52 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><?= date('i',strtotime($model->hospitalisation_datetime));?><BR>
					</P>
				</TD>
				<TD WIDTH=213 STYLE="border-top: 1px solid #000000; border-bottom: none; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Відділення <SPAN style="text-decoration: underline;"><?= Yii::t('common',$model->departments->dept_name);?></SPAN></FONT></P>
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">палата № <SPAN style="text-decoration: underline;"><?= $model->chamber;?></SPAN></FONT></P>
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
			</TR>
			<TR VALIGN=TOP>
				<TD WIDTH=115 HEIGHT=8 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT>                     
					<FONT SIZE=2 STYLE="font-size: 9pt">дата</FONT></P>
				</TD>
				<TD COLSPAN=7 WIDTH=205 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">(число,
					    місяць,   рік)</FONT></P>
				</TD>
				<TD COLSPAN=3 WIDTH=109 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">(годин,
					хвилин)</FONT></P>
				</TD>
				<TD WIDTH=213 STYLE="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
			</TR>
		</TBODY>
		<TBODY>
			<TR VALIGN=TOP>
				<TD COLSPAN=2 WIDTH=116 HEIGHT=8 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=30 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=18 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=14 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=24 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=24 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=24 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD COLSPAN=2 WIDTH=43 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=52 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
					</P>
				</TD>
				<TD WIDTH=213 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in">
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">В
					поточному році з           </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
					</SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt; text-decoration: underline;">Ο вперше</FONT></P>
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">приводу
					даної  хвороби  </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
					  </SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt">Ο повторно</FONT></P>
					<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">госпіталізований
					            </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
					 </SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"> всього___раз</FONT></P>
				</TD>
			</TR>
		</TBODY>
		<TBODY style="padding: 0px;">
			<TR>
				<TD COLSPAN=12 WIDTH=700 VALIGN=TOP STYLE="border-top: none; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in">
					<TABLE WIDTH=684 CELLPADDING=7 CELLSPACING=0 BORDER=NONE>
						<COL WIDTH=350>
						<COL WIDTH=350>
						<TR>
							<TD WIDTH=350 >
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Проведено ліжко-днів&nbsp;<SPAN style="text-decoration: underline;"><?= ($model->bed_days) ? round($model->bed_days) : '______________________________  ';?></SPAN></FONT></P>
							</TD>
							<TD WIDTH=350 >
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Переведений(а) у відділення&nbsp;<SPAN style="text-decoration: underline;"><?= ($model->movedtodepartments->dept_name) ? $model->movedtodepartments->dept_name : '_____________________________' ;?></SPAN></FONT></P></SPAN></FONT></P>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
			<TR>
				<TD COLSPAN=12 WIDTH=700 VALIGN=TOP STYLE="border-top: none; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in">
					<TABLE WIDTH=684 CELLPADDING=7 CELLSPACING=0 BORDER=NONE>
						<COL WIDTH=260>
						<COL WIDTH=260>
						<COL WIDTH=150>
						<TR>
							<TD WIDTH=250 >
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Група крові&nbsp;<SPAN style="text-decoration: underline;"><?= ($model->blood_type_id) ? $model->blood_type_id : '___________________'?></SPAN></FONT></P>
							</TD>
							<TD WIDTH=270 >
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Резус приналежність&nbsp;<SPAN style="text-decoration: underline;"><?= ($model->rhesus_affilation) ? $model->rhesus_affilation : '__________________________';?></SPAN></FONT></P>
							</TD>
							<TD WIDTH=150 >
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">RW&nbsp;<SPAN style="text-decoration: underline;"><?= ($model->rw_examination) ? date('d-m-Y',strtotime($model->rw_examination)) : '___________________________';?></SPAN></FONT></P>
								<P style="text-align: right; font-size:9pt;">(число, місяць, рік)</P>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>

			<TR>
				<TD COLSPAN=12 WIDTH=700 VALIGN=TOP STYLE="border-top: none; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in">
					<TABLE WIDTH=684 CELLPADDING=7 CELLSPACING=0 BORDER=NONE>
						<COL WIDTH=260>
						<TR>
							<TD WIDTH=700>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Підвищена чутливість або непереносимість препарату &nbsp;<SPAN style="text-decoration: underline;"><?= ($model->side_effects) ? $model->side_effects : '_____________________________________________________________';?></SPAN></FONT></P>
								<P style="font-size: 9pt;">(найменування препарату, характер побічної дії)</P>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>		
			<TR>
				<TD COLSPAN=12 WIDTH=700 VALIGN=TOP STYLE="border-top: none; border-bottom: 1.50pt solid #000000; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in;">
					<TABLE WIDTH=700 CELLPADDING=7 CELLSPACING=0 BORDER=NONE style="margin: 0; padding: 0;">
						
						<TR WIDTH=700>
							<TD WIDTH=700>
								<!--<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt; font-weight: bold;">1. Прізвище, ім’я, по батькові &nbsp;<SPAN style="text-decoration: underline;"><?= $patient->last_name . $patient->first_name . $patient->middle_name;?></SPAN></FONT></P>-->
								<P LANG="uk-UA" CLASS="western" ALIGN=JUSTIFY><FONT SIZE=2 STYLE="font-size: 9pt"><B>1. Прізвище, ім’я, по батькові &nbsp;<SPAN style="text-decoration: underline;"><?= $patient->last_name . $patient->first_name . $patient->middle_name;?></SPAN></B></FONT></P>
							</TD>
						</TR>
						<TR>
							<TD>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">2. Стать:
								ч-1, ж-2 <SPAN style="padding: 2px 0; border: 1px solid black;"><?= ($patient->gender_val == 0) ? '2':'1';?></SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
								 </SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"> </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
								</SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"> </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
								</SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"> 3.
								Вік <?= ($patient->date_of_birth) ? $patient->date_of_birth: '';?>&nbsp;(повних
								років, для дітей: до 1-го року – місяців;
								до 1-го місяця – днів)</FONT></P>
								<P LANG="uk-UA" CLASS="western" ALIGN=CENTER>                    
								                                                                
								<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><FONT SIZE=2 STYLE="font-size: 9pt"></FONT></P>
							</TD>
						</TR>
						<TR>
							<TD>
								<P LANG="uk-UA" CLASS="western" ALIGN=JUSTIFY><FONT SIZE=2 STYLE="font-size: 9pt">4.
								Постійне місце проживання: </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
								</SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"> </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
								</SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt">
								<?= ($patient->address)? '<SPAN style="font-size:9pt; text-decoration:underline;">
								'.$patient->address.'</SPAN>':'_______________________________________'?></FONT></P>

								<P LANG="uk-UA" CLASS="western" ALIGN=CENTER>                    
								                                                          
								<FONT SIZE=2 STYLE="font-size: 9pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(вписати адресу:
								область, район, населений пункт)</FONT></P>
								<P LANG="uk-UA" CLASS="western" ALIGN=JUSTIFY><BR>
								</P>
								
							</TD>
						</TR>
						<TR style="padding: 0px; margin: 0px;" >
							<TD style="border-bottom: 1px solid black; padding: 0px; margin: 0px; text-align: center;">
							<P LANG="uk-UA" CLASS="western" ALIGN=JUSTIFY ><FONT SIZE=2 STYLE="font-size: 9pt"><?= ($patient->phone) ? '<SPAN style="font-size:9pt; text-decoration:underline;">Основний телефон: 
								'.$patient->phone.'</SPAN>' :'____________________________________________________________________________________________________________'?></FONT></P>
							</TD>
						</TR>
						<TR style="padding: 0px; margin: 0px;">
							<TD style=" padding: 0px; margin: 0px; text-align: center;">
								
								<P LANG="uk-UA" CLASS="western" ALIGN=CENTER>        №
								<FONT SIZE=2 STYLE="font-size: 9pt">телефону; для
								приїжджих – адресу родичів</FONT></P>
								
							</TD>
						</TR>
						<TR>
							<TD>
								<P LANG="uk-UA" CLASS="western" ALIGN=JUSTIFY><FONT SIZE=2 STYLE="font-size: 9pt">5.
								Місце роботи, спеціальність або
								посада <?= ($patient->job_position) ? '<SPAN style="text-decoration:underline;">'.$patient->job_position.'</SPAN>' :'________________________________________________________________________';?>
								</FONT>
								</P>
							</TD>
						</TR>
						<TR style="padding: 0; margin: 0;">
							<TD style="text-align: center; padding: 0; margin: 0; border-bottom: 1px solid black;">
							<P LANG="uk-UA" CLASS="western" ALIGN=JUSTIFY><FONT SIZE=2 STYLE="font-size: 9pt">
							<?= ($patient->place_of_work) ? '<SPAN style="text-decoration:underline;">'.$patient->place_of_work.'</SPAN>':'' ?>
							&nbsp;
							<?= ($patient->pilgova_grupa) ? '<SPAN style="text-decoration:underline;">'.$patient->pilgova_grupa.'</SPAN>':'' ?>
							&nbsp;
							<?= ($patient->pilgova_cat) ? '<SPAN style="text-decoration:underline;">'.$patient->pilgova_cat.'</SPAN>':'' ?>
							&nbsp;
							</FONT></P>
							</TD>
						</TR>
						<TR style="padding: 0; margin: 0;">
							<TD style="text-align: center; padding: 0; margin: 0;">
								<P LANG="uk-UA" CLASS="western" style="font-size:9pt;">(для учнів, студентів
								-  місце навчання; для дітей -  назву
								дитячого закладу, школи; для інвалідів
								– вид і група інвалідності, ІВВ –
								так, ні (підкреслити)</P>

							</TD>
						</TR>
						<TR>
							<TD>
								<P LANG="uk-UA" CLASS="western" STYLE="font-size: 9pt">6. Ким направлений
								хворий <?= ($model->sent_by_clinic) ? '<SPAN style="text-decoration: underline;">'.$model->sent_by_clinic.'</SPAN>' : '____________________________________________________________________________________'; ?>
								</P>
							</TD>
						</TR>
						<TR style="padding: 0; margin: 0;">
							<TD style="text-align: center; padding: 0; margin: 0;">
								<P LANG="uk-UA" CLASS="western" ALIGN=CENTER><FONT SIZE=2 STYLE="font-size: 9pt">(найменування
								лікувального закладу)</FONT></P>
							</TD>
						</TR>
						<TR>
							<TD>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">7.
								Госпіталізований(а) в стаціонар: за
								<?= ($model->hospitalisation_variants_id == '1') ? '<SPAN style="text-decoration:underline;">терміновими показаннями – 1</SPAN>': 'терміновими показаннями – 1';?>,
								<?= ($model->hospitalisation_variants_id == '2') ?'<SPAN style="text-decoration:underline;">через</SPAN>':'через';?><?= ($model->hospitalisation_time) ? '<SPAN style="text-decoration:underline;">'.$model->hospitalisation_time.'</SPAN>':'________';?> <?= ($model->hospitalisation_variants_id == '2')?'<SPAN style="text-decoration:underline;">годин на початку захворювання,
								одержання травми; в плановому порядку
								– 2</SPAN>':'годин на початку захворювання,
								одержання травми; в плановому порядку
								– 2';?>   </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
								</SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"> </FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN LANG="ru-RU">
								</SPAN></FONT><FONT SIZE=2 STYLE="font-size: 9pt"><SPAN style="padding: 2px 0; border: 1px solid black;"><?= ($model->hospitalisation_variants_id == 1) ? '1':'2';?></FONT></P>
							</TD>
						</TR>
						<TR>
							<TD>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">8.
								Діагноз лікувального закладу, який
								направив хворого:
								<?= ($model->diagnosis_default) ? '<SPAN style="text-decoration:underline;">'.$model->diagnosis_default.'</SPAN>' :'___________________________________________________________'?></FONT></P>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
								</P>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">9.
								Діагноз при
								госпіталізації <?= ($model->diagnosis_hospitalisation) ? '<SPAN style="text-decoration:underline;">'.$model->diagnosis_hospitalisation.'</SPAN>' :'___________________________________________________________'?></FONT></P>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><BR>
								</P>
								<P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">10.
								Діагноз
								клінічний <?= ($model->diagnosis_clinical) ? '<SPAN style="text-decoration:underline;">'.$model->diagnosis_clinical.'</SPAN>' :'___________________________________________________________'?></FONT></P>
							</TD>
						</TR>
						<TR WIDTH=700>
							<TD  style="padding: 0; margin: 0;"><P LANG="uk-UA" CLASS="western" ALIGN=LEFT><FONT SIZE=2 STYLE="font-size: 9pt">Дата встановлення: <?= ($model->diagnosis_clinical_date) ? '<SPAN style="text-decoration:underline;">'.$model->diagnosis_clinical_date.'</SPAN> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '_____________________________________________';?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Лікар <?= ($model->diagnosis_clinical_doctor) ? '<SPAN style="text-decoration:underline;">'.$model->diagnosis_clinical_doctor.'</SPAN>' : '_____________________________________________';?>
							</FONT></P></TD>
						</TR>
					</TABLE>
				</TD>
			</TR>		
			</TBODY>
	</TABLE>
</BODY>