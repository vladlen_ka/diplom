<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Departments;
use yii\bootstrap\Tabs;
use app\models\Gender;
use app\models\PatientCard;
/* @var $this yii\web\View */
/* @var $model app\models\MedicalCardForm */

$this->title = Yii::t('patient_medical_form', 'Medical card №') . $model->medical_card_number;
if(Yii::$app->user->can('viewPost')) {
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Patient Cards'), 'url' => ['patient-card/index']];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-card-form-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  if($model->closed_datetime != NULL) {?>
        <div class="alert alert-info">
            <strong><?= Yii::t('patient_medical_form', 'Info!')?></strong><span><?= Yii::t('patient_medical_form', 'Card closed datetime : ').$model->closed_datetime;?></span>
        </div>
    <?php }?>
    <?php if($patient){
        $date = date('Y-m-d H:i:s');
        $age = date('Y') - date('Y', strtotime($patient->date_of_birth));

        if($age == '0') {
            $age = date('m') - date('m', strtotime($patient->date_of_birth));
            if ($age == '0') {
                $age =  (floor((strtotime($date)  - strtotime($patient->date_of_birth)) / (60*60*24)));
                if($age == '0') {
                    $age = (floor((strtotime($date)  - strtotime($patient->date_of_birth)) / (60*60)));
                    if($age == '0') {
                        $age = (floor((strtotime($date)  - strtotime($patient->date_of_birth)) / (60)));
                        $age_val = Yii::t('common', 'minutes');
                    } else {
                        $age_val = Yii::t('common', 'hours');
                    }
                } else {
                    $age_val = Yii::t('common', 'days');
                }
            } else {
               $age_val = Yii::t('common', 'months');
            }
        } else {
            $age_val = Yii::t('common', 'years');
        }
        
        echo '<span style="font-size:16px; font-weight:bold;">' . $patient->last_name . ' ' . $patient->first_name . ' ' . $patient->middle_name . ' ' . Yii::t('common', 'Age: ') .  $age . ' ' . $age_val . ' '.  Yii::t('common', 'Gender:') . ' ' . $patient->gender->gender_value .'</span>';
        
    }?>
    <?php
        if(Yii::$app->user->can('createPost') && ($model->closed_datetime == NULL || $model->closed_datetime == '0000-00-00 00:00:00') && $epikriz == false) {
            echo Html::a(
                Yii::t('epikriz', 'Create Epikriz'),
                ['epikriz/create', 'patient_id' => Yii::$app->request->get('patient_id'), 'medical_card_id' => Yii::$app->request->get('id')], 
                ['class' => 'btn btn-primary align-right pull-right']
            );
            echo "<br/>";
            echo "<br/>";
            echo Html::a(
                Yii::t('patalogoanatomy', 'Create Patalogoanatomy Note'),
                ['anatomopathological/create', 'patient_id' => Yii::$app->request->get('patient_id'), 'medical_card_id' => Yii::$app->request->get('id')], 
                ['class' => 'btn btn-primary align-right pull-right']
            );
        } elseif(Yii::$app->user->can('viewPost') || (PatientCard::getUserIdByCardId(Yii::$app->request->get('patient_id')) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {
            if($epikriz) {
                echo Html::a(
                    Yii::t('epikriz', 'View Epikriz'),
                    ['epikriz/view', 'id' => $epikriz->id, 'patient_id' => Yii::$app->request->get('patient_id'), 'medical_card_id' => Yii::$app->request->get('id')], 
                    ['class' => 'btn btn-info pull-right']
                );
            } else if($anatomopathological) {
                echo Html::a(
                    Yii::t('patalogoanatomy', 'View Patalogoanatomy Note'),
                    ['anatomopathological/view', 'id' => $anatomopathological->id, 'patient_id' => Yii::$app->request->get('patient_id'), 'medical_card_id' => Yii::$app->request->get('id')], 
                    ['class' => 'btn btn-info pull-right']
                );
            }
        }
        
    ?>

    <?=
        Tabs::widget([
            'items' => [
                [  
                    'label' => Yii::t('patient_medical_form', 'General info'),
                    'content' => $this->render('tabs/_general_info', ['model' => $model]),
                    'active' => true // указывает на активность вкладки
                ],
                [
                    'label' => Yii::t('patient_medical_form','Diagnoses'),
                    'content' => $this->render('tabs/_diagnoses', ['model' => $model]),
                ],
                [
                    'label' => Yii::t('patient_medical_form', 'Conclusion'),
                    'content' => $this->render('tabs/_conclusion_info', ['model' => $model]),
                    //'active' => true // указывает на активность вкладки
                ],
                [
                    'label' => Yii::t('patient_medical_form', 'Receive Doctor Notes'),
                    'content' => $this->render('tabs/_receive_notes', ['medical_data' => $receive_doctor_note]),
                ],
                [
                    'label' => Yii::t('patient_medical_form', 'Diary'),
                    'content' => $this->render('tabs/_diary', ['medical_data' => $diary]),
                ],
                [
                    'label' => Yii::t('patient_medical_form', 'Additional'),
                    'items' => [
                        [
                            'label' => Yii::t('patient_medical_form', 'Surgeries'),
                            'content' => $this->render('tabs/_surgeries_form', ['medical_data' => $surgeries]),
                        ],
                        [
                            'label' => Yii::t('patient_medical_form', 'Work Incapacity Lists'),
                            'content' => $this->render('tabs/_work_incapacity_lists', ['medical_data' => $work_incapacity_lists]),
                        ],
                    ],
                ],
            ],
        ]);
    ?>

    <?php /*echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            
            
            'other_medical_services_malignant_formation_id',
            'work_incapacity_certificate_id',
        ],
    ]) */?>

</div>
