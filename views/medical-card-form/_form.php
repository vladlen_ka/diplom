<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use app\models\Departments;
use app\models\Lang;
use app\models\UserIdentity;
use app\models\ClassMkb;
use app\models\Workabilities;
use app\models\Outcomes;
use app\models\ServiceMalignantFormations;

use yii\bootstrap\Tabs;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $model app\models\MedicalCardForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="medical-card-form-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--<?= $form->field($model, 'returns_time')->textInput() ?>-->

    <?= $model->isNewRecord ?$form->field($model, 'hospitalisation_datetime')->textInput(['value' => date('Y-m-d H:i:s'), 'readonly' => true]) : $form->field($model, 'hospitalisation_datetime')->textInput(['readonly' => true]) ?>
    <?php
        echo Tabs::widget([
            'items' => [
                [  
                    'label' => Yii::t('patient_medical_form', 'General info'),
                    'content' => 
                                $form->field($model, 'medical_card_number')->textInput(['maxlength' => true]) .

                                $form->field($model, 'department_id')->dropDownList(Common::transarray(ArrayHelper::map(Departments::find()->all(), 'dept_id', 'dept_name')),['prompt' => Yii::t('patient_medical_form','Select department')]) .

                                $form->field($model, 'chamber')->textInput(['maxlength' => true, 'placeholder' => Yii::t('patient_medical_form', 'Input chamber')]) .
                                
                                

                                $form->field($model, 'move_to_department_id')->dropDownList(Common::transarray(ArrayHelper::map(Departments::find()->all(), 'dept_id', 'dept_name')),['prompt' => Yii::t('patient_medical_form','Select department')]) .

                                $form->field($model, 'blood_type_id')->dropDownList(Common::transarray([
                                    '1' =>  Yii::t('patient_medical_form','0 (I) - first type'),
                                    '2' =>  Yii::t('patient_medical_form','A (II) - second type'),
                                    '3' =>  Yii::t('patient_medical_form','B (III) - third type'),
                                    '4' =>  Yii::t('patient_medical_form','AB (IV) - fourth type'),
                                ]),['prompt' => Yii::t('patient_medical_form', 'Choose Blood Type')]) .

                                $form->field($model, 'rhesus_affilation')->dropDownList(Common::transarray([
                                    '0' =>  Yii::t('patient_medical_form','Rh(-) - negative'),
                                    '1' =>  Yii::t('patient_medical_form','Rh(+) - positive'),

                                ]),['prompt' => Yii::t('patient_medical_form', 'Choose Rhesus')]) .

                                $form->field($model, 'rw_examination')->widget(\yii\jui\DatePicker::classname(), [
                                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                                    'dateFormat' => 'yyyy-MM-dd',
                                    'clientOptions' => [
                                        'changeMonth' => true,
                                        'yearRange' => '1991:2099',
                                        'changeYear' => true,
                                    ],
                                    'options' => ['class' => 'form-control', 'placeholder' => Yii::t('patient_medical_form', 'Select RW Date')],
                                ]) .

                                $form->field($model, 'vil_examination')->widget(\yii\jui\DatePicker::classname(), [
                                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                                    'dateFormat' => 'yyyy-MM-dd',
                                    'clientOptions' => [
                                        'changeMonth' => true,
                                        'yearRange' => '1991:2099',
                                        'changeYear' => true,
                                    ],
                                    'options' => ['class' => 'form-control', 'placeholder' => Yii::t('patient_medical_form', 'Select Vil Date')],
                                ]) .
                                $form->field($model, 'side_effects')->textarea(['rows' => 3]) .
                                $form->field($model, 'sent_by_clinic')->textInput(['maxlength' => true]) .

                                '<div class="form-group field-medicalcardform-hospitalisation_variants_id">
                                    <label class="control-label">' . Yii::t('patient_medical_form', 'Hospitalisation Variants') .'</label>
                                    <input type="hidden" name="MedicalCardForm[hospitalisation_variants_id]" value=""><div id="medicalcardform-hospitalisation_variants_id" class="form-checkboxes"><label><input type="radio" name="MedicalCardForm[hospitalisation_variants_id]" value="1">' . Yii::t('patient_medical_form', 'Urgent indications') .'</label>
                                    <label><input type="radio" name="MedicalCardForm[hospitalisation_variants_id]" value="2">&nbsp;<input type="text" id="medicalcardform-hospitalisation_time" class="checkbox_input" name="MedicalCardForm[hospitalisation_time]" maxlength="10">' . Yii::t('patient_medical_form', ' hours after onset of the disease, an injury; routinely') .'  </label></div>

                                    <div class="help-block"></div>
                                </div>'
                    ,
                    'active' => true // указывает на активность вкладки
                ],
                [
                    'label' => Yii::t('patient_medical_form','Diagnoses'),
                    'content' => 

                                $form->field($model, 'diagnosis_default')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => ClassMkb::getDiagnoses(),
                                    ],
                                ]) .

                                $form->field($model, 'diagnosis_hospitalisation')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => ClassMkb::getDiagnoses(),
                                    ],
                                ]) .

                                $form->field($model, 'diagnosis_clinical')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => ClassMkb::getDiagnoses(),
                                    ],
                                ]) .

                               /* $form->field($model, 'diagnosis_clinical_date')->widget(\yii\jui\DatePicker::classname(), [
                                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                                    'dateFormat' => 'yyyy-MM-dd',
                                    'clientOptions' => [
                                        'changeMonth' => true,
                                        'yearRange' => '1991:2099',
                                        'changeYear' => true,
                                    ],
                                    'options' => ['class' => 'form-control', 'placeholder' => Yii::t('patient_medical_form', 'Select Diagnosis Clinical Date')],
                                ]) .*/

                                $form->field($model, 'diagnosis_clinical_doctor')->dropDownList(ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),['prompt' => Yii::t('patient_medical_form','Select doctor')]) .

                                $form->field($model, 'diagnosis_final')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => ClassMkb::getDiagnoses(),
                                    ],
                                ]) .

                                $form->field($model, 'diagnosis_final_main')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => ClassMkb::getDiagnoses(),
                                    ],
                                ]) .

                                $form->field($model, 'diagnosis_final_main_complication')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => ClassMkb::getDiagnoses(),
                                    ],
                                ]) .

                                $form->field($model, 'diagnosis_final_concomitant')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => ClassMkb::getDiagnoses(),
                                    ],
                                ]) 
                                ,
                ],
                [
                    'label' => Yii::t('patient_medical_form', 'Conclusion'),
                    'content' => 
                                $form->field($model, 'other_medical_services')->textarea(['rows' => 6]) .

                                $form->field($model, 'other_medical_services_malignant_formation_id')->radioList(Common::transarray(ArrayHelper::map(ServiceMalignantFormations::getValues(), 'id', 'value')),['class' => 'form-checkboxes']) .

                                $form->field($model, 'workability_id')->radioList(Common::transarray(ArrayHelper::map(Workabilities::getWorkabilities(), 'id', 'value')),['class' => 'form-checkboxes']) .

                                $form->field($model, 'conclusion_expertise')->textarea(['rows' => 6]) .

                                $form->field($model, 'outcome_id')->radioList(Common::transarray(ArrayHelper::map(Outcomes::getOutcomes(), 'id', 'value')),['class' => 'form-checkboxes']) .

                                $form->field($model, 'notes')->textarea(['rows' => 6]) 
                                ,
                ],
            ]
        ]);
    ?>
.

    <?= $form->field($model, 'doctor')->dropDownList(ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),['prompt' => Yii::t('patient_medical_form','Select doctor')]) ?>

    <?= $form->field($model, 'department_head')->dropDownList(ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),['prompt' => Yii::t('patient_medical_form','Select doctor')]) ?>
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
