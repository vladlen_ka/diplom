<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MedicalCardForm */

$this->title = Yii::t('patient_medical_form', 'Create Medical Card Form');
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_card', 'Patient Cards'), 'url' => ['patient-card/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_card', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="medical-card-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'patient' => $patient,
    ]) ?>

</div>
