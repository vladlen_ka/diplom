<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

use dosamigos\datetimepicker\DateTimePicker;

use app\models\Lang;
use app\models\Departments;
use app\models\UserIdentity;
use app\models\PatientCard;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MedicalCardFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="medical-card-form-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php 
            if(\Yii::$app->user->can('createPatient')) { 
               echo Html::a(
                    Yii::t('patient_medical_form', 'Create Medical Card Form'),
                    ['create', 'patient_id' => Yii::$app->request->get('id')], 
                    ['class' => 'btn btn-success']
                ); 
            } 
        ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'medical_card_number',
            [
                'attribute' => 'hospitalisation_datetime',
                'value'     => 'hospitalisation_datetime',
                'format' => 'raw',
                'options' => ['class'=>'several-datepicker-filters'],
                'filter' => DateTimePicker::widget([
                    'model'=> $searchModel,
                    'attribute'=>'hospitalisation_datetime_from',
                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                    'clientOptions' => [
                        'yearRange' => '1991:2099',
                        'format' => 'yyyy-mm-dd HH:ii:ss',
                        'todayBtn' => true
                    ],
                    'options' => ['placeholder' => Yii::t('patient_card', 'From'), 'class' => 'form-control datepicker-filter'],
                ]) . ' ' . DateTimePicker::widget([
                    'model'=> $searchModel,
                    'attribute'=>'hospitalisation_datetime_to',
                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                    'clientOptions' => [
                        'yearRange' => '1991:2099',
                        'format' => 'yyyy-mm-dd HH:ii:ss',
                        'todayBtn' => true
                    ],
                    'options' => ['placeholder' => Yii::t('patient_card', 'To'), 'class' => 'form-control datepicker-filter'],
                ]) ,

            ],
            [
                'label' => Yii::t('patient_card', 'Departments Dept Name'),
                'attribute' => 'departments.dept_name',
                'value' => function($model) {
                    return Common::transelement($model->departments->dept_name);
                },
                'format' => 'html',
                'filter' => Html::activeDropDownList(
                    $searchModel, 
                    'departments.dept_id',
                    Common::transarray(ArrayHelper::map(Departments::getDepartments(), 'dept_id', 'dept_name')),
                    ['class' => 'form-control', 'prompt' => Yii::t('patient_medical_form', 'Select department')]
                ),
            ],
            [
                'label' => Yii::t('patient_card', 'Doctor Full Name'),
                'attribute' => 'doctor_full_name',
                'value' => function ($model) {
                    return $model->user->getFullName();
                },
                'filter' =>Html::activeDropDownList(
                    $searchModel, 
                    'user.id',
                    ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),
                    ['class' => 'form-control', 'prompt' => Yii::t('patient_medical_form', 'Select doctor')]
                ),
            ],
            [
               'class' => \yii\grid\ActionColumn::className(),
               'buttons'=>[
                    'view'=>function ($url, $model) {
                        if(\Yii::$app->user->can('viewPost') || (PatientCard::getUserIdByCardId(Yii::$app->request->get('id')) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['medical-card-form/view','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl, ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                        }
                    },
                    'update' => function ($url, $model) {
                        if(\Yii::$app->user->can('updatePost') && $model['closed_datetime'] == NULL) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['medical-card-form/update','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl, ['title' => Yii::t('yii', 'Update'), 'data-pjax' => '0']);
                        }
                    },
                    'delete' => function ($url, $model) {
                        if(\Yii::$app->user->can('deletePost') && $model['closed_datetime'] == NULL     && isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin'])) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['medical-card-form/delete','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl, ['title' => Yii::t('yii', 'Delete'), 'data-pjax' => '0', 'data-method'=>'post']);
                        }
                    },
                    'open_card' => function ($url, $model) {
                        if(\Yii::$app->user->can('createUser') && $model['closed_datetime'] != NULL  && isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin'])) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['medical-card-form/open-card','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-open"></span>', $customurl, ['title' => Yii::t('common', 'Open Card'), 'data-pjax' => '0', 'data-method'=>'post']);
                        }
                    },

                    /*'create_pdf' => function ($url, $model) {
                        if(\Yii::$app->user->can('viewPost')) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['medical-card-form/pdf','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-print"></span>', $customurl, ['title' => Yii::t('common', 'Create PDF'), 'data-pjax' => '0', 'data-method'=>'post']);
                        }
                    },*/
                    'close_card' => function ($url, $model) {
                        if(\Yii::$app->user->can('createUser') && $model['closed_datetime'] == NULL && isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin'])) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['medical-card-form/close-card','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-save"></span>', $customurl, ['title' => Yii::t('yii', 'Close Card'), 'data-pjax' => '0', 'data-method'=>'post']);
                        }
                    }
                ],
               'template'=>'{view} {update} {delete} {open_card} {close_card}',
           ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
