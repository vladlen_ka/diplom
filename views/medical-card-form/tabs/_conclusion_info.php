<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Departments; 
use app\models\UserIdentity;
use app\models\Workabilities;
use app\models\Outcomes;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $model app\models\MedicalCardForm */

?>

 <?= 
 	DetailView::widget([
	     'model' => $model,
	     'attributes' => [
            'other_medical_services:ntext',
            [
                  'label' => Yii::t('patient_medical_form','Workability'),
                  'value' => Common::transelement(Workabilities::getWorkabilityById($model->workability_id)),
            ],
            'conclusion_expertise:ntext',
            [
                  'label' => Yii::t('patient_medical_form','Outcome'),
                  'value' => Common::transelement(Outcomes::getOutcomeById($model->outcome_id)),
            ],
            'notes:ntext',
            [
            	'label' => Yii::t('patient_medical_form','Responsible Doctor'),
            	'value' => UserIdentity::getDoctorById($model->doctor),
            ],
            [
            	'label' => Yii::t('patient_medical_form','Department Head'),
            	'value' => UserIdentity::getDoctorById($model->department_head),
            ],
    	]
 	]); 
 ?>