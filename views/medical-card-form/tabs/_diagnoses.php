<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Departments;
use app\models\UserIdentity;
/* @var $this yii\web\View */
/* @var $model app\models\MedicalCardForm */

?>

 <?= 
 	DetailView::widget([
		'model' => $model,
	    'attributes' => [
	        'diagnosis_default:ntext',
            'diagnosis_hospitalisation:ntext',
            'diagnosis_clinical:ntext',
            'diagnosis_clinical_date',
            [
            	'label' => Yii::t('patient_medical_form','Diagnosis Doctor'),
            	'value' => UserIdentity::getDoctorById($model->diagnosis_clinical_doctor),
            ],
            'diagnosis_final:ntext',
            'diagnosis_final_main:ntext',
            'diagnosis_final_main_complication:ntext',
            'diagnosis_final_concomitant:ntext',
    	]
 	]); 
 ?>