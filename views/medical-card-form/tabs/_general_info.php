<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use app\models\Departments;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $model app\models\MedicalCardForm */

?>
 <?= 
 	DetailView::widget([
		'model' => $model,
	    'attributes' => [
	        'medical_card_number',
	        'hospitalisation_datetime',
	        [
                  'label' => Yii::t('patient_medical_form','Department'),
                  'value' => Common::transelement(Departments::getDepartmentById($model->department_id)),
            ],
	        'chamber',
	        //'returns_time:datetime',
	        'bed_days',
	        [
                  'label' => Yii::t('patient_medical_form','Move To Department'),
                  'value' => Common::transelement(Departments::getDepartmentById($model->move_to_department_id)),
            ],
	        'blood_type_id',
	        'rhesus_affilation',
	        'rw_examination',
	        'vil_examination',
	        'sent_by_clinic',
	        'hospitalisation_variants_id',
	        'side_effects:ntext',
    	]
 	]); 
 ?>