<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MedicalCardFormSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="medical-card-form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'patient_card_id') ?>

    <?= $form->field($model, 'medical_card_number') ?>

    <?= $form->field($model, 'hospitalisation_datetime') ?>

    <?= $form->field($model, 'department_id') ?>

    <?php // echo $form->field($model, 'chamber') ?>

    <?php // echo $form->field($model, 'returns_time') ?>

    <?php // echo $form->field($model, 'bed_days') ?>

    <?php // echo $form->field($model, 'move_to_department_id') ?>

    <?php // echo $form->field($model, 'blood_type_id') ?>

    <?php // echo $form->field($model, 'rhesus_affilation') ?>

    <?php // echo $form->field($model, 'rw_examination') ?>

    <?php // echo $form->field($model, 'vil_examination') ?>

    <?php // echo $form->field($model, 'side_effects') ?>

    <?php // echo $form->field($model, 'hospitalisation_variants_id') ?>

    <?php // echo $form->field($model, 'hospitalisation_time') ?>

    <?php // echo $form->field($model, 'diagnosis_default') ?>

    <?php // echo $form->field($model, 'diagnosis_hospitalisation') ?>

    <?php // echo $form->field($model, 'diagnosis_clinical') ?>

    <?php // echo $form->field($model, 'diagnosis_clinical_date') ?>

    <?php // echo $form->field($model, 'diagnosis_clinical_doctor') ?>

    <?php // echo $form->field($model, 'diagnosis_final') ?>

    <?php // echo $form->field($model, 'diagnosis_final_main') ?>

    <?php // echo $form->field($model, 'diagnosis_final_main_complication') ?>

    <?php // echo $form->field($model, 'diagnosis_final_concomitant') ?>

    <?php // echo $form->field($model, 'surgeries_id_list') ?>

    <?php // echo $form->field($model, 'other_medical_services') ?>

    <?php // echo $form->field($model, 'other_medical_services_malignant_formation_id') ?>

    <?php // echo $form->field($model, 'work_incapacity_certificate_id') ?>

    <?php // echo $form->field($model, 'workability_id') ?>

    <?php // echo $form->field($model, 'conclusion_expertise') ?>

    <?php // echo $form->field($model, 'outcome_id') ?>

    <?php // echo $form->field($model, 'notes') ?>

    <?php // echo $form->field($model, 'doctor') ?>

    <?php // echo $form->field($model, 'department_head') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('patient_medical_form', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('patient_medical_form', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
