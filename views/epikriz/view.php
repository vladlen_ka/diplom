<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MedicalCardForm;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $model app\models\Epikriz */

$this->title = Yii::t('epikriz', 'Epikrizs');
if(Yii::$app->user->can('viewPost')) {
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Patient Cards'), 'url' => ['patient-card/index']];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Medical card №') . $medical_data->medical_card_number, 'url' => ['medical-card-form/view', 'id'=>Yii::$app->request->get('medical_card_id'), 'patient_id' => Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="epikriz-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'notes:ntext',
            'blood:ntext',
            'pee:ntext',
            'liver:ntext',
            'coprogram:ntext',
            'blood_glukose:ntext',
            'pee_amilase:ntext',
            'pee_diastase:ntext',
            'pee_glukose:ntext',
            [
                'label' => Yii::t('patient_medical_form', 'Diagnosis Final Main'),
                'value' => MedicalCardForm::getDiagnosisFinalMainById(Yii::$app->request->get('medical_card_id')),
            ],
            [
                'label' => Yii::t('patient_medical_form', 'Diagnosis Final Main Complication'),
                'value' => MedicalCardForm::getDiagnosisFinalMainComplicationById(Yii::$app->request->get('medical_card_id')),
            ],
            [
                'label' => Yii::t('patient_medical_form', 'Diagnosis Final Concomitant'),
                'value' => MedicalCardForm::getDiagnosisFinalMainConcomitantById(Yii::$app->request->get('medical_card_id')),
            ],
            'healing:ntext',
            'healing_and_recommendations:ntext',
            [
                'label' => Yii::t('epikriz', 'Scabies Review'),
                'value' => Common::transelement($model->scabies_review),
            ],
            [
                'label' => Yii::t('epikriz', 'Pediculosis'),
                'value' => Common::transelement($model->pediculosis),
            ],
            'date_created',
        ],
    ]) ?>

</div>
