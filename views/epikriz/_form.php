<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\Common;
use app\models\ClassMkb;
use app\models\Outcomes;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Epikriz */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
    echo Html::a(
        Yii::t('common', 'Back'),
        ['medical-card-form/view', 'id' => Yii::$app->request->get('medical_card_id'), 'patient_id' => Yii::$app->request->get('patient_id')], 
        ['class' => 'btn btn-primary align-right pull-right']
    );
?>
<br>
<div class="epikriz-form">

    

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_created')->textInput(['value' => date('Y-m-d H:i:s'), 'readonly' => true]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'blood')->textInput() ?>

    <?= $form->field($model, 'pee')->textInput() ?>

    <?= $form->field($model, 'liver')->textInput() ?>

    <?= $form->field($model, 'coprogram')->textInput() ?>

    <?= $form->field($model, 'blood_glukose')->textInput() ?>

    <?= $form->field($model, 'pee_amilase')->textInput() ?>

    <?= $form->field($model, 'pee_diastase')->textInput() ?>

    <?= $form->field($model, 'pee_glukose')->textInput() ?>

    <?php 
        if($medical_data->diagnosis_final_main != NULL) {
            echo $form->field($medical_data, 'diagnosis_final_main')->textInput(['value' => $medical_data->diagnosis_final_main, 'readonly' => true])->label(Yii::t('patient_medical_form', 'Diagnosis Final Main'));
        } else {
            echo $form->field($medical_data, 'diagnosis_final_main')->widget(\yii\jui\AutoComplete::classname(), [
                'options' => ['class' => 'form-control'],
                'clientOptions' => [
                    'source' => ClassMkb::getDiagnoses(),
                ],
            ])->label(Yii::t('patient_medical_form', 'Diagnosis Final Main'));
        }
    ?>

    <?php 
        if($medical_data->diagnosis_final_main_complication != NULL) {
            echo $form->field($medical_data, 'diagnosis_final_main_complication')->textInput(['value' => $medical_data->diagnosis_final_main_complication, 'readonly' => true])->label(Yii::t('patient_medical_form', 'Diagnosis Final Main Complication'));
        } else {
            echo $form->field($medical_data, 'diagnosis_final_main_complication')->widget(\yii\jui\AutoComplete::classname(), [
                'options' => ['class' => 'form-control'],
                'clientOptions' => [
                    'source' => ClassMkb::getDiagnoses(),
                ],
            ])->label(Yii::t('patient_medical_form', 'Diagnosis Final Main Complication'));
        }
    ?>

    <?php 
        if($medical_data->diagnosis_final_concomitant != NULL) {
            echo $form->field($medical_data, 'diagnosis_final_concomitant')->textInput(['value' => $medical_data->diagnosis_final_concomitant, 'readonly' => true])->label(Yii::t('patient_medical_form', 'Diagnosis Final Concomitant'));
        } else {
            echo $form->field($medical_data, 'diagnosis_final_concomitant')->widget(\yii\jui\AutoComplete::classname(), [
                'options' => ['class' => 'form-control'],
                'clientOptions' => [
                    'source' => ClassMkb::getDiagnoses(),
                ],
            ])->label(Yii::t('patient_medical_form', 'Diagnosis Final Concomitant'));
        }
    ?>
    <?= $form->field($medical_data, 'outcome_id')->radioList(Common::transarray(ArrayHelper::map(Outcomes::getOutcomes(), 'id', 'value')),['class' => 'form-checkboxes']) ?>

    <?= $form->field($model, 'healing')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'healing_and_recommendations')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'scabies_review')->radioList(Common::transarray(['yes' => 'yes', 'no' =>'no'], ['class' => 'from-checkboxes'])); ?>
    <?= $form->field($model, 'pediculosis')->radioList(Common::transarray(['yes' => 'yes', 'no' =>'no'], ['class' => 'from-checkboxes'])); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Create'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
