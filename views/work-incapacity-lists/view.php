<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WorkIncapacityLists */

$this->title = Yii::t('work_incapacity_lists', 'Work Incapacity List') . ' №' . $model->number;
if(Yii::$app->user->can('viewPost')) {
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Patient Cards'), 'url' => ['patient-card/index']];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Medical card №') . $medical_data->medical_card_number, 'url' => ['medical-card-form/view', 'id'=>Yii::$app->request->get('medical_card_id'), 'patient_id' => Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-incapacity-lists-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number',
            'datetime_from',
            'datetime_to',
        ],
    ]) ?>

</div>
