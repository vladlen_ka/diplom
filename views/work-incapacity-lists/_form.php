<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lang;
use yii\helpers\ArrayHelper;
use app\models\UserIdentity;
/* @var $this yii\web\View */
/* @var $model app\models\WorkIncapacityLists */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-incapacity-lists-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'datetime_from')->widget(\dosamigos\datetimepicker\DateTimePicker::classname(), [
        'model'=> $model,
        'attribute'=>'datetime_from',
        'language' => (Lang::getCurrent()->url == 'ua') ? 'en' : Lang::getCurrent()->url,
        'clientOptions' => [
            'yearRange' => '1991:2099',
            'format' => 'yyyy-mm-dd HH:ii:ss',
            'todayBtn' => true
        ],
        'options' => ['placeholder' => Yii::t('work_incapacity_lists', 'Datetime From'), 'class' => 'form-control'],
    ]) ?>

    <?= $form->field($model, 'datetime_to')->widget(\dosamigos\datetimepicker\DateTimePicker::classname(), [
        'model'=> $model,
        'attribute'=>'datetime_to',
        'language' => (Lang::getCurrent()->url == 'ua') ? 'en' : Lang::getCurrent()->url,
        'clientOptions' => [
            'yearRange' => '1991:2099',
            'format' => 'yyyy-mm-dd HH:ii:ss',
            'todayBtn' => true
        ],
        'options' => ['placeholder' => Yii::t('work_incapacity_lists', 'Datetime To'), 'class' => 'form-control'],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
