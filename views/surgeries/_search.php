<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SurgeriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="surgeries-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'medical_card_form_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'datetime') ?>

    <?= $form->field($model, 'anesthesia_method') ?>

    <?php // echo $form->field($model, 'complication') ?>

    <?php // echo $form->field($model, 'doctor_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('surgeries', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('surgeries', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
