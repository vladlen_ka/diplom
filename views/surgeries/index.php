<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\UserIdentity;
use yii\widgets\Pjax;
use dosamigos\datetimepicker\DateTimePicker;
use app\models\Lang;
use app\models\MedicalCardForm;
use app\models\PatientCard;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SurgeriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>
<div class="surgeries-index">

    <p>
        <?php 
        if (\Yii::$app->user->can('createPost') && MedicalCardForm::getCardClosedTimeById(Yii::$app->request->get('id'))) {
         echo Html::a(Yii::t('surgeries', 'Create Surgeries'), ['create', 'medical_card_id' => Yii::$app->request->get('id'), 'patient_id' => Yii::$app->request->get('patient_id')], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
   <?php Pjax::begin(); ?>  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'name',
            [
                'attribute' => 'datetime',
                'value'     => 'datetime',
                'format' => 'raw',
                'options' => ['class'=>'several-datepicker-filters'],
                'filter' => DateTimePicker::widget([
                    'model'=> $searchModel,
                    'attribute'=>'datetime_from',
                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                    'clientOptions' => [
                        'yearRange' => '1991:2099',
                        'format' => 'yyyy-mm-dd H:i:s',
                        'todayBtn' => true
                    ],
                    'options' => ['placeholder' => Yii::t('patient_card', 'From'), 'class' => 'form-control datepicker-filter'],
                ]) . ' ' . DateTimePicker::widget([
                    'model'=> $searchModel,
                    'attribute'=>'datetime_to',
                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                    'clientOptions' => [
                        'yearRange' => '1991:2099',
                        'format' => 'yyyy-mm-dd H:i:s',
                        'todayBtn' => true
                    ],
                    'options' => ['placeholder' => Yii::t('patient_card', 'To'), 'class' => 'form-control datepicker-filter'],
                ]) ,

            ],
            [
                'attribute' => 'doctor_full_name',
                'label' => Yii::t('patient_card', 'Doctor Full Name'),
                'value' => function ($model) {
                    if($model->user) {
                        return $model->user->getFullName();
                    }
                },
                'filter' =>Html::activeDropDownList(
                    $searchModel, 
                    'user.id',
                    ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),
                    ['class' => 'form-control', 'prompt' => Yii::t('patient_medical_form', 'Select doctor')]
                ),
            ],

            [
               'class' => \yii\grid\ActionColumn::className(),
               'buttons'=>[
                    'view'=>function ($url, $model) {
                        if((\Yii::$app->user->can('viewPost') && MedicalCardForm::getCardClosedTimeById(Yii::$app->request->get('id'))) || (PatientCard::getUserIdByCardId(Yii::$app->request->get('patient_id')) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['surgeries/view','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('patient_id'), 'medical_card_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl, ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                        }
                    },
                    'update' => function ($url, $model) {
                        if(\Yii::$app->user->can('updatePost') && MedicalCardForm::getCardClosedTimeById(Yii::$app->request->get('id'))) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['surgeries/update','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('patient_id'), 'medical_card_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl, ['title' => Yii::t('yii', 'Update'), 'data-pjax' => '0']);
                        }
                    },
                    'delete' => function ($url, $model) {
                        if(\Yii::$app->user->can('deletePost') && MedicalCardForm::getCardClosedTimeById(Yii::$app->request->get('id'))) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['surgeries/delete','id'=>$model['id'] , 'patient_id' => Yii::$app->request->get('patient_id'), 'medical_card_id' => Yii::$app->request->get('id')]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl, ['title' => Yii::t('yii', 'Delete'), 'data-pjax' => '0', 'data-method'=>'post']);
                        }
                    },
                ],
               'template'=>'{view} {update} {delete}',
           ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
