<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lang;
use yii\helpers\ArrayHelper;
use app\models\UserIdentity;
/* @var $this yii\web\View */
/* @var $model app\models\Surgeries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="surgeries-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'datetime')->widget(\dosamigos\datetimepicker\DateTimePicker::classname(), [
        'model'=> $model,
        'attribute'=>'datetime',
        'language' => (Lang::getCurrent()->url == 'ua') ? 'en' : Lang::getCurrent()->url,
        'clientOptions' => [
            'yearRange' => '1991:2099',
            'format' => 'yyyy-mm-dd HH:ii:ss',
            'todayBtn' => true
        ],
        'options' => ['placeholder' => Yii::t('surgeries', 'Surgery Datetime'), 'class' => 'form-control'],
    ]) ?>

    <?= $form->field($model, 'anesthesia_method')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'complication')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'doctor_id')->dropDownList(ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),['class' => 'form-control', 'prompt' => Yii::t('patient_medical_form', 'Select doctor')]); 
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
