<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReceiveDoctorNoteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="receive-doctor-note-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'medical_card_form_id') ?>

    <?= $form->field($model, 'complaints') ?>

    <?= $form->field($model, 'disease_anamnesis') ?>

    <?= $form->field($model, 'life_anamnesis') ?>

    <?php // echo $form->field($model, 'patient_objective_condition') ?>

    <?php // echo $form->field($model, 'datetime_added') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('receive_doctor_note', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('receive_doctor_note', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
