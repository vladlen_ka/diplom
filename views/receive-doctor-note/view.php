<?php


use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\UserIdentity;
/* @var $this yii\web\View */
/* @var $model app\models\ReceiveDoctorNote */

$this->title = Yii::t('receive_doctor_note', 'Receive Doctor Note');
if(Yii::$app->user->can('viewPost')) {
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Patient Cards'), 'url' => ['patient-card/index']];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Medical card №') . $medical_data->medical_card_number, 'url' => ['medical-card-form/view', 'id'=>Yii::$app->request->get('medical_card_id'), 'patient_id' => Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receive-doctor-note-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'complaints:ntext',
            'disease_anamnesis:ntext',
            'life_anamnesis:ntext',
            'patient_objective_condition:ntext',
            [
                'label' => Yii::t('receive_doctor_note','Responsible Doctor'),
                'value' => UserIdentity::getDoctorById($model->doctor_id),
            ],
        ],
    ]) ?>

</div>
