<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lang;
use yii\helpers\ArrayHelper;
use app\models\UserIdentity;

/* @var $this yii\web\View */
/* @var $model app\models\ReceiveDoctorNote */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="receive-doctor-note-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'complaints')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'disease_anamnesis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'life_anamnesis')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'patient_objective_condition')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'doctor_id')->dropDownList(ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),['class' => 'form-control', 'prompt' => Yii::t('patient_medical_form', 'Select doctor')]); 
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
