<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PatientCard */

$this->title = Yii::t('patient_card', 'Update PatientCard: ') . $model->last_name . ' ' . $model->first_name . ' ' . $model->middle_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_card', 'Patient Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->last_name . ' ' . $model->first_name . ' ' . $model->middle_name, 'url' => ['view', 'id' => $model->patient_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-card-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
