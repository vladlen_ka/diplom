<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\Gender;
use app\models\PatientCard;
use yii\jui\DatePicker;
use app\models\Lang;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PatientCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('patient_card', 'Patient Cards');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-card-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
            if (\Yii::$app->user->can('createPatient')) {
                echo   Html::a(Yii::t('patient_card', 'Create Patient Card'), ['create'], ['class' => 'btn btn-success']); 
            }
        ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'first_name',
            'last_name',
            'middle_name',
            [
                'attribute' => 'gend',
                'label'     => Yii::t('patient_card','Gender'),
                'value'    => 'gender.gender_value',
                'filter' =>Html::activeDropDownList(
                    $searchModel, 
                    'gender.gender_id',
                    ArrayHelper::map(Gender::getGender(), 'gender_id', 'gender_value'),
                    ['class' => 'form-control', 'prompt' => Yii::t('patient_card', 'Select gender')]
                ),
            ],
            [
                'attribute' => 'date_of_birth',
                'value'     => 'date_of_birth',
                'format' => 'html',
                'options' => ['class'=>'several-datepicker-filters'],
                'filter' => DatePicker::widget([
                    'model'=> $searchModel,
                    'attribute'=>'date_of_birth_from',
                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'yearRange' => '1991:2099',
                        'changeYear' => true,
                    ],
                    'options' => ['placeholder' => Yii::t('patient_card', 'From'), 'class' => 'form-control datepicker-filter'],
                ]) . ' ' . DatePicker::widget([
                    'model'=> $searchModel,
                    'attribute'=>'date_of_birth_to',
                    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'yearRange' => '1991:2099',
                        'changeYear' => true,
                    ],
                    'options' => ['placeholder' => Yii::t('patient_card', 'To'), 'class' => 'form-control datepicker-filter'],
                ]),

            ],
            
            [
               'class' => \yii\grid\ActionColumn::className(),
               'buttons'=>[
                    'view'=>function ($url, $model) {
                        if(\Yii::$app->user->can('viewPatient')) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['patient-card/view','id'=>$model['patient_id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl, ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                        }
                    },
                    'update' => function ($url, $model) {
                        if(\Yii::$app->user->can('updatePatient')) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['patient-card/update','id'=>$model['patient_id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl, ['title' => Yii::t('yii', 'Update'), 'data-pjax' => '0']);
                        }
                    },
                    'delete' => function ($url, $model) {
                        if(\Yii::$app->user->can('deletePatient')) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['patient-card/delete','id'=>$model['patient_id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl, ['title' => Yii::t('yii', 'Delete'), 'data-pjax' => '0', 'data-method'=>'post']);
                        }
                    },
                ],
               'template'=>'{view} {update} {delete}',
           ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
