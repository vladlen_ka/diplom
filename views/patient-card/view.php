<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\PatientCard */

$this->title = $model->first_name . ' ' . $model->last_name . ' ' . $model->middle_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_card', 'Patient Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-card-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
    echo Tabs::widget([
        'items' => [
            [  
                'label' => Yii::t('patient_card', 'General info'),
                'content' => $this->render('tabs/_general_info.php', ['model' => $model]),
                'active' => true // указывает на активность вкладки
            ],
            [
                'label' => Yii::t('patient_card','Contacts'),
                'content' => $this->render('tabs/_address', ['model' => $model]),
            ],
            [
                'label' => Yii::t('patient_card', 'Forms'),
                'content' => $this->render('tabs/_medical_form', ['medical_data' => $medical_card]),
            ],
        ]
    ]);
    ?>

</div>
