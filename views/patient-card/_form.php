<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;

use yii\jui\DatePicker;

use app\models\Lang;
use app\models\Gender;
/* @var $this yii\web\View */
/* @var $model app\models\PatientCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patient-card-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender_val')->dropDownList(ArrayHelper::map(Gender::getGender(), 'gender_id', 'gender_value'),['prompt' => Yii::t('patient_card','Select gender')]); ?>

    <?= $form->field($model, 'date_of_birth')->widget(\yii\jui\DatePicker::classname(), [
        'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
        'dateFormat' => 'yyyy-MM-dd',
        'clientOptions' => [
            'changeMonth' => true,
            'yearRange' => '1991:2099',
            'changeYear' => true,
        ],
        'options' => ['class' => 'form-control'],
    ]) ?>

    <?= $form->field($model, 'image')->fileInput() ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 4]) ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+38(999)999-9999',
    ]) ?>

    <?= $form->field($model, 'phone_additional')->widget(MaskedInput::className(), [
        'mask' => '+38(999)999-9999',
    ]) ?>

    <?= $form->field($model, 'place_of_work')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pilgova_grupa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pilgova_cat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pilgova_series')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pilogva_number')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
