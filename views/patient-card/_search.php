<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PatientCardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patient-card-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'card_id') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'middle_name') ?>

    <?= $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'date_of_birth') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'phone_additional') ?>

    <?php // echo $form->field($model, 'place_of_work') ?>

    <?php // echo $form->field($model, 'job_position') ?>

    <?php // echo $form->field($model, 'pilgova_grupa') ?>

    <?php // echo $form->field($model, 'pilgova_cat') ?>

    <?php // echo $form->field($model, 'pilgova_series') ?>

    <?php // echo $form->field($model, 'pilogva_number') ?>

    <?php // echo $form->field($model, 'medical_card_id_list') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('patient_card', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('patient_card', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
