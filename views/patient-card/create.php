<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PatientCard */

$this->title = Yii::t('patient_card', 'Create Patient Card');
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_card', 'Patient Cards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patient-card-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
