<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PatientCard */
?>
<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'address:ntext',
            'phone',
            'phone_additional',
            'place_of_work',
            'job_position',
        ],
]) ?>