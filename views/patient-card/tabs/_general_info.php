<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PatientCard */
?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        //'patient_id',
        'first_name',
        'last_name',
        'middle_name',
        'gender.gender_value',
        'date_of_birth:date',
        [
            'attribute'=>'photo',
            'value'=> $model->photo,
            'format' => ['image', ['width' => '150', 'height' => '150']],
        ],
        'pilgova_grupa',
        'pilgova_cat',
        'pilgova_series',
        'pilogva_number',
    ],
]) ?>