<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\CompareDiagnosis;
use app\models\DifferenceReason;
use app\components\Common;
use app\models\UserIdentity;
/* @var $this yii\web\View */
/* @var $model app\models\Anatomopathological */

$this->title = Yii::t('patalogoanatomy','Patatalogoanatome №').$model->number;
if(Yii::$app->user->can('viewPost')) {
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Patient Cards'), 'url' => ['patient-card/index']];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Medical card №') . $medical_data->medical_card_number, 'url' => ['medical-card-form/view', 'id'=>Yii::$app->request->get('medical_card_id'), 'patient_id' => Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="anatomopathological-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'number',
            'adres:ntext',
            'date',
            'diagnosis_main:ntext',
            'diagnosis_complication:ntext',
            'diagnosis_concomitant:ntext',
            [
                'label' => Yii::t('anatomopathological', 'Compare Diagnosis'),
                'value' => function($model) {
                    return Common::transelement(CompareDiagnosis::getDiagnosisByid($model->clinical_patalogoanatomy_compare));
                },
            ],
            [
                'label' => Yii::t('anatomopathological', 'Difference Reason'),
                'value' => function($model) {
                    return Common::transelement(DifferenceReason::getDiagnosisByid($model->clinical_patalogoanatomy_compare));
                },
            ],
            'death_reason_number',
            'death_reason_text:ntext',
            'death_illness:ntext',
            'main_illness:ntext',
            'other_notes:ntext',
            [
                'label' => Yii::t('anatomopathological','Patalogoanatomist'),
                'value' => UserIdentity::getDoctorById($model->doctor),
            ],
            [
                'label' => Yii::t('anatomopathological','Patalogoanatomy Department Head'),
                'value' => UserIdentity::getDoctorById($model->department_head),
            ],
        ],
    ]) ?>

</div>
