<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ClassMkb;
use yii\helpers\ArrayHelper;
use app\models\UserIdentity;
use app\models\CompareDiagnosis;
use app\models\DifferenceReason;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $model app\models\Anatomopathological */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="anatomopathological-form">
<?php $diagnoses = ClassMkb::getDiagnoses();?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'adres')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date')->textInput(['value' => date('Y-m-d'), 'readonly' => true]) ?>

    <?= $form->field($model, 'diagnosis_main')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => $diagnoses,
                                        'minLength'=>'3',
                                    ],
                                ]); ?>

    <?= $form->field($model, 'diagnosis_complication')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => $diagnoses,
                                        'minLength'=>'3',
                                    ],
                                ]); ?>

    <?= $form->field($model, 'diagnosis_concomitant')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' =>  $diagnoses,
                                        'minLength'=>'3',
                                    ],
                                ]); ?>

    <?= $form->field($model, 'clinical_patalogoanatomy_compare')->radioList(Common::transarray(ArrayHelper::map(CompareDiagnosis::getDiagnoses(), 'id', 'value')),['class' => 'form-checkboxes']) ?>

    <?= $form->field($model, 'difference_reason')->radioList(Common::transarray(ArrayHelper::map(DifferenceReason::getDiagnoses(), 'id', 'value')),['class' => 'form-checkboxes']) ?>

    <?= $form->field($model, 'death_reason_number')->textInput() ?>

    <?= $form->field($model, 'death_reason_text')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'death_illness')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'main_illness')->widget(\yii\jui\AutoComplete::classname(), [
                                    'options' => ['class' => 'form-control'],
                                    'clientOptions' => [
                                        'source' => $diagnoses,
                                        'minLength'=>'3',
                                    ],
                                ]); ?>

    <?= $form->field($model, 'other_notes')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'doctor')->dropDownList(ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),['prompt' => Yii::t('patient_medical_form','Select doctor')]) ?>

    <?= $form->field($model, 'department_head')->dropDownList(ArrayHelper::map(UserIdentity::getDoctors(), 'id', 'fullName'),['prompt' => Yii::t('patient_medical_form','Select doctor')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('yii', 'Create'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
