<?php

/* @var $this yii\web\View */

$this->title = Yii::t('home','Statistics');
use yii\bootstrap\Tabs;
use app\models\Lang;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
?>
<?php 
if($model) {
	$count = 0;
	foreach ($model as $key => $value) {
		$items_table[] = array(
			'label' => $key,
			'content' => $this->render('tabs/_stat_table', ['data' => $value,'doctor_card_stat' => $doctor_card_stat[$key]]),
			//'active' => ($count == 0) ? true : false,
		);
		$count++;
	}
} else {
	$items = [['label'=> '', 'content'=>'',]];
}
?>
<div class="row">	
<?php ActiveForm::begin(['action' => [''], 'method' => 'get']); ?>
	<div class="col-sm-4">
	<label><?= Yii::t('home', 'Period From')?></label>
		<?= DateTimePicker::widget([
		    'name' => 'datetime_from',
		    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
		    'size' => 'ms',
		    'value' => Yii::$app->request->get('datetime_from'),
		    'clientOptions' => [
		        'autoclose' => true,
		        'yearRange' => '1991:2099',
                'format' => 'yyyy-mm-dd HH:ii:ss',
		        'todayBtn' => true
		    ],
		    'options' => ['placeholder' => Yii::t('patient_card', 'From'), 'class' => 'form-control'],
		]);?>
	</div>
	<div class="col-sm-4">
	<label><?= Yii::t('home', 'Period To')?></label>
		<?= DateTimePicker::widget([
		    'name' => 'datetime_to',
		    'language' => (Lang::getCurrent()->url == 'ua')? 'en' : Lang::getCurrent()->url,
		    'size' => 'ms',
		    'value' => Yii::$app->request->get('datetime_to'),
		    'clientOptions' => [
		        'autoclose' => true,
		        'yearRange' => '1991:2099',
                'format' => 'yyyy-mm-dd HH:ii:ss',
		        'todayBtn' => true
		    ],
		    'options' => ['placeholder' => Yii::t('patient_card', 'To'), 'class' => 'form-control'],
		]);?>
	</div>
	<div class="form-group">
	<br>
        <?= Html::submitButton(Yii::t('home','Find'), ['class'=>'btn btn-primary']) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>
<br/>
<?=  Tabs::widget([
            'items' => [
                [
                    'label' => Yii::t('home', 'Choose Doctor'),
                    'items' => $items_table,
                ],
                [
                	'label' => Yii::t('home', 'Protocol Fulfillment'),
                	'content' => $this->render('tabs/protocol_fulfillment', ['data' => $doctor_stat]),
                	'active' => true,
                ],
                [
                	'label' => Yii::t('home', 'Chart'),
                	'content' => $this->render('tabs/_chart', ['data' => $chart_data]),
                ],
            ],
        ]);
?>