<?php
	use yii\bootstrap\Tabs;

if($data) {
	$count = 0;
	foreach ($data as $key => $value) {
		$items_table[] = array(
			'label' => Yii::t('home', 'Card Number: ').$key,
			'content' => $this->render('_table', ['data' => $value]),
			'active' => ($count == 0) ? true : false,
		);
		$count++;
	}
} else {
	$items = [['label'=> '', 'content'=>'',]];
}
?>
<br/>
<?=  Tabs::widget([
            'items' => [
                [
                    'label' => Yii::t('home', 'Choose Card'),
                    'items' => $items_table,
                ],
                [
                	'label' => Yii::t('home', 'Protocol Card Fulfillment'),
                	'content' => $this->render('protocol_card_fulfillment', ['data' => $doctor_card_stat]),
                ],
            ],
        ]);
?>