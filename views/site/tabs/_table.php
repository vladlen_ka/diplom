<div class="table-responsive">
  <table class="table table-bordered table-condensed table-hover">
  	<thead>
  		<tr>
	  		<th>№</th>
	  		<th style="width:65%"><?= Yii::t('home', 'Quality Indicator'); ?></th>
	  		<th style="width:11%"><?= Yii::t('home', 'Yes'); ?></th>
	  		<th style="width:11%"><?= Yii::t('home', 'No'); ?></th>
	  		<th style="width:11%"><?= Yii::t('home', 'Empty'); ?></th>
  		</tr>
  	</thead>
    <tbody>
    	<tr>
    		<td>1</td>
    		<td><?= Yii::t('home','Provided the original review.');?></td>
    		
    		<td align="center"><?= ($data['n1'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n1'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n1'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    	<tr>
    		<td>2</td>
    		<td><?= Yii::t('home','Initiation of treatment.');?></td>
    		
    		<td align="center"><?= ($data['n2'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n2'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n2'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    	<tr>
    		<td>3</td>
    		<td><?= Yii::t('home','Set the clinical diagnosis.');?></td>
    		
    		<td align="center"><?= ($data['n3'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n3'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n3'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    	<tr>
    		<td>4</td>
    		<td><?= Yii::t('home','There are daily entries in the diary.');?></td>
    		
    		<td align="center"><?= ($data['n4'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n4'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n4'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    	<tr>
    		<td>5</td>
    		<td><?= Yii::t('home','All diaries completed all required fields.');?></td>
    		
    		<td align="center"><?= ($data['n5'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n5'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n5'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    	<tr>
    		<td>6</td>
    		<td><?= Yii::t('home','Having outcome document.');?></td>
    		
    		<td align="center"><?= ($data['n6'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n6'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n6'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>

    	<tr>
    		<td>7</td>
    		<td><?= Yii::t('home','Collection of complaints.');?></td>
    		
    		<td align="center"><?= ($data['n7'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n7'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n7'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    	<tr>
    		<td>8</td>
    		<td><?= Yii::t('home','Collection of history of illness.');?></td>
    		
    		<td align="center"><?= ($data['n8'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n8'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n8'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>

    	<tr>
    		<td>9</td>
    		<td><?= Yii::t('home','Collection of life history.');?></td>
    		
    		<td align="center"><?= ($data['n9'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n9'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n9'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    	<tr>
    		<td>10</td>
    		<td><?= Yii::t('home','Setting the objective of the patient.');?></td>
    		
    		<td align="center"><?= ($data['n10'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n10'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n10'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>

    	<tr>
    		<td>11</td>
    		<td><?= Yii::t('home','Department is set.');?></td>
    		
    		<td align="center"><?= ($data['n11'] === true) ? '<span class="glyphicon glyphicon-ok"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n11'] === false) ? '<span class="glyphicon glyphicon-remove"></span>' : '' ?></td>
    		<td align="center"><?= ($data['n11'] === null) ? '<span class="glyphicon glyphicon-warning-sign"></span>' : '' ?></td>
    	</tr>
    </tbody>
  </table>
</div>
