
<div class="table-responsive">
  <table class="table table-bordered table-condensed table-hover">
  	<thead>
  		<tr>
	  		<th>№</th>
	  		<th><?= Yii::t('home', 'Card'); ?></th>
	  		<th><?= Yii::t('home', 'Result (%)'); ?></th>
  		</tr>
  	</thead>
    <tbody>
        <?php $count = 1;?>
    	<?php foreach($data as $card => $result) {?>
            <tr>
                <td><?= $count;?></td>
                <td><?= $card;?></td>
                <td><?= $result;?></td>
            </tr>
        <?php $count++;?>
        <?php }?>
    </tbody>
  </table>
</div>
