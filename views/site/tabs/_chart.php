<?php
	$data_x = array(['x']);
	$data_y = array(['Result']);
	
	$count = 0;
	foreach ($data as $date => $value) {
		array_push($data_x, $date);
		array_push($data_y, $value);
		$count++;
	}	
?>

<h2><?= Yii::t('home', 'Middle General')?></h2>
<?php echo \yii2mod\c3\chart\Chart::widget([
    'options' => [
            'id' => 'popularity_chart'
    ],
    'clientOptions' => [
       'data' => [
            'x' => 'x',
            'columns' => [
                $data_x,
            	$data_y,
            ],
            'types' => [
            	'Result' => 'area-spline',
            ],
            'colors' => [
                'Popularity' => '#4EB269',
            ],
        ],
        'axis' => [
            'x' => [
                'label' => Yii::t('home','Days'),
                'type' => 'timeseries',
                'tick' => [
                	'count' => $count,
                	'format' => '%Y-%m-%d',
                ],
            ],
            'y' => [
                'label' => [
                    'text' => Yii::t('home','Protocol Fulfillment') . '%',
                    'position' => 'outer-middle'
                ],
                'min' => 0,
                'max' => 100,
                'padding' => ['top' => 10, 'bottom' => 0]
            ]
        ]
    ]
]); ?>