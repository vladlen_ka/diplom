<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\PatientCard;
?>

<?php

$radioList_arr = array(
	'1' => '1',
	'2' => '2',
	'3' => '4',
	'5' => '5',
	'6' => '6',
	'7' => '7',
	'8' => '8',
	'9' => '9',
	'10' => '10',
);
?>
<div class="container">
<h3 style="text-align: center;"><?= Yii::t('home','Welcome to medical information system');?></h3>

<?php if($model != NULL) {?>
<label><?= Yii::t('home', 'Please leave vote about medical care level:');?></label>
<br/>
<?php $form = ActiveForm::begin(); ?>
	<div class="row">
			
	<?= $form->field($model, 'patient_vote')->radioList($radioList_arr)->label(Yii::t('home', 'Medical Card № ').$model->medical_card_number); ?>
		

		<div class="form-group">
		    <?= Html::submitButton(Yii::t('home', 'Проголосовать') , ['class' =>'btn btn-success']) ?>
		</div>
	</div>
<?php ActiveForm::end(); ?>
<?php } else {?>
	<?= Html::a(Yii::t('main_menu', 'My profile'), ['/patient-card/view', 'id' => PatientCard::getIdByUserId(Yii::$app->user->identity->id)], ['class' => 'btn btn-info btn-lg']) ?>
<?php }?>
</div>
