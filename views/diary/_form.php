<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lang;
use yii\helpers\ArrayHelper;
use app\models\UserIdentity;
/* @var $this yii\web\View */
/* @var $model app\models\Diary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diary-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pulse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'arterial_pressure')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'temprature')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'breathing')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'drink_quantity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'daily_pee')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emptying')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('yii', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
