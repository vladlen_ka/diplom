<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\UserIdentity;
/* @var $this yii\web\View */
/* @var $model app\models\Diary */

$this->title = Yii::t('diary', 'Diary Note: ').$model->datetime_added;
if(Yii::$app->user->can('viewPost')) {
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Patient Cards'), 'url' => ['patient-card/index']];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Medical card №') . $medical_data->medical_card_number, 'url' => ['medical-card-form/view', 'id'=>Yii::$app->request->get('medical_card_id'), 'patient_id' => Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diary-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'datetime_added',
            'pulse',
            'arterial_pressure',
            'temprature',
            'breathing',
            'weight',
            'drink_quantity',
            'daily_pee',
            'emptying',
            'bath',
            'comments:ntext',
            [
                'label' => Yii::t('surgeries','Doctor'),
                'value' => UserIdentity::getDoctorById($model->doctor_id),
            ],
        ],
    ]) ?>

</div>
