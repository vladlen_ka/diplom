<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DiarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="diary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'medical_card_form_id') ?>

    <?= $form->field($model, 'datetime_added') ?>

    <?= $form->field($model, 'pulse') ?>

    <?= $form->field($model, 'arterial_pressure') ?>

    <?php // echo $form->field($model, 'temprature') ?>

    <?php // echo $form->field($model, 'breathing') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'drink_quantity') ?>

    <?php // echo $form->field($model, 'daily_pee') ?>

    <?php // echo $form->field($model, 'emptying') ?>

    <?php // echo $form->field($model, 'bath') ?>

    <?php // echo $form->field($model, 'comments') ?>

    <?php // echo $form->field($model, 'doctor_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('diary', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('diary', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
