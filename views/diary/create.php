<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Diary */

$this->title = Yii::t('diary', 'Create Diary');
if(Yii::$app->user->can('viewPost')) {
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_card', 'Patient Cards'), 'url' => ['patient-card/index']];
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', $patient->first_name . ' ' . $patient->last_name . ' ' . $patient->middle_name), 'url' => ['patient-card/view', 'id'=>Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('patient_medical_form', 'Medical card №') . $medical_data->medical_card_number, 'url' => ['medical-card-form/view', 'id'=>Yii::$app->request->get('medical_card_id'), 'patient_id' => Yii::$app->request->get('patient_id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="diary-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
