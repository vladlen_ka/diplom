<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\WLang;
use app\models\PatientCard;
use app\models\UserIdentity;
use yii\web\Controller;
AppAsset::register($this);
?>
<?php 
if (Yii::$app->session->get('access_token') != NULL) {
    $access_token = UserIdentity::getAccessToken(Yii::$app->user->identity->id);
    if ($access_token != Yii::$app->session->get('access_token')) {

        Yii::$app->session->destroy();
        Yii::$app->user->logout();
        Yii::$app->session->set('kicked_reason', Yii::t('common', 'Someone else logged in your account'));
        return Controller::goHome();
    }
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('main_menu','Medical Informational System'),
        'brandUrl' => ['site/index'],
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo WLang::widget();
    $user = isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['basic']);
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('main_menu','Dashboard'), 'url' => ['/site/index']],
            ['label' => Yii::t('main_menu', 'About'), 'url' => ['/site/about']],
            ['label' => Yii::t('main_menu', 'My profile'), 'url' => ['/patient-card/view', 'id' => PatientCard::getIdByUserId(Yii::$app->user->identity->id)], 'visible' => $user],
            ['label' => Yii::t('main_menu','Patient Cards'), 'url' => ['/patient-card/index'], 'visible' => Yii::$app->user->can('viewPatient')],
            ['label' => Yii::t('main_menu','Users'), 'url' => ['/user/index'], 'visible' => Yii::$app->user->can('viewUser')],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    Yii::t('main_menu','Logout').' (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
   
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
