<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\authAssignment;
use app\components\Common;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php 
            if(\Yii::$app->user->can('createUser')) { 
               echo Html::a(Yii::t('user', 'Create User'), ['create'], ['class' => 'btn btn-success']); 
            } 
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fullName',
            'username',
             [
                'attribute' => 'role',
                'label'     => Yii::t('user','Role'),
                //'value' => 'authItem.description',
                'value' => function($model) {
                   return Yii::t('common',$model->authItem->description);
                },
                'filter' =>Html::activeDropDownList(
                    $searchModel, 
                    'authAssignment.item_name',
                    Common::transarray(ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')),
                    ['class' => 'form-control', 'prompt' => Yii::t('user', 'Select role')]
                ),

            ],
            [
               'class' => \yii\grid\ActionColumn::className(),
               'buttons'=>[
                    'view'=>function ($url, $model) {
                        if(\Yii::$app->user->can('viewUser')) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['user/view','id'=>$model['id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl, ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                        }
                    },
                    'update' => function ($url, $model) {
                        if(\Yii::$app->user->can('updateUser')) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['user/update','id'=>$model['id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $customurl, ['title' => Yii::t('yii', 'Update'), 'data-pjax' => '0']);
                        }
                    },
                    'delete' => function ($url, $model) {
                        if(\Yii::$app->user->can('deleteUser')) {
                            $customurl=Yii::$app->getUrlManager()->createUrl(['user/delete','id'=>$model['id']]); //$model->id для AR
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $customurl, ['title' => Yii::t('yii', 'Delete'), 'data-pjax' => '0', 'data-method'=>'post']);
                        }
                    },
                ],
               'template'=>'{view} {update} {delete}',
           ],
        ],
    ]); ?>
</div>
