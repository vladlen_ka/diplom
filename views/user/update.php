<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\AuthItem;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('user', 'Update user:	') . $model->last_name . ' ' . $model->first_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->last_name . ' ' . $model->first_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('user', 'Update');

$user_roles = Yii::$app->authManager->getRoles(); 
$current_user_role = Yii::$app->authManager->getRolesByUser($model->id);
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    
	<div class="user-form">

	    <?php $form = ActiveForm::begin(); ?>
	    <?php if (!isset($current_user_role['basic'])) {?>
		    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

	    	<?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>
	    <?php } else {?>
		    <?= $form->field($model, 'last_name')->textInput(['disabled' => true]) ?>

		    <?= $form->field($model, 'first_name')->textInput(['disabled' => true]) ?>

	    	<?= $form->field($model, 'middle_name')->textInput(['disabled' => true]) ?>
	    <?php }?>
	    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

	    <?php 
		    if (!isset($current_user_role['basic'])) {
		    	if (isset($user_roles['basic'])) {
			     	unset($user_roles['basic']);
			    }
	    ?>
	    	<?= $form->field($roles, 'item_name')->dropDownList(Common::transarray(ArrayHelper::map($user_roles, 'name', 'description')),['prompt' => Yii::t('user', 'Choose user role')]); ?>
	    <?php } else {?>
	    	<?= $form->field($roles, 'item_name')->dropDownList(Common::transarray(ArrayHelper::map($user_roles, 'name', 'description')),['style' => 'display: none','prompt' => Yii::t('user', 'Choose user role')]); ?>
	    <?php }?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>

</div>
