<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\AuthItem;
use app\components\Common;
/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('user', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    
	<div class="user-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

	    <?php 
	    	$roles = Yii::$app->authManager->getRoles(); 
	    	if (isset($roles['basic'])) {
		     	unset($roles['basic']);
		     }
	     ?>
	    <?= $form->field($model, 'user_role')->dropDownList(Common::transarray(ArrayHelper::map($roles, 'name', 'description')),['prompt' => Yii::t('user','Choose user role')]); ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>

</div>
