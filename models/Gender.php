<?php

namespace app\models;

use Yii;
use app\models\PatientCard;
/**
 * This is the model class for table "gender".
 *
 * @property integer $gender_id
 * @property string $gender_value
 */
class Gender extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gender';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gender_id', 'gender_value'], 'required'],
            [['gender_id'], 'integer'],
            [['gender_value'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gender_id' => Yii::t('patient_card', 'Gender ID'),
            'gender_value' => Yii::t('patient_card', 'Gender Value'),
        ];
    }

    public function getPatientCard() {
        return $this->hasMany(PatientCard::className(), ['gender_id' => 'gender_val']);
    }

    public function getGender() {
        return Gender::find()->indexBy('gender_id')->all();
    }

    public function getGenderById($id) {
        return Gender::find($id)->one()['gender_value'];
    }
}
