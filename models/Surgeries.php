<?php

namespace app\models;

use Yii;
use app\models\UserIdentity;
use app\models\MedicalCardForm;
/**
 * This is the model class for table "surgeries".
 *
 * @property integer $id
 * @property integer $medical_card_form_id
 * @property string $name
 * @property string $datetime
 * @property string $anesthesia_method
 * @property string $complication
 * @property integer $doctor_id
 */
class Surgeries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surgeries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['medical_card_form_id', 'doctor_id'], 'integer'],
            [['datetime'], 'safe'],
            [['complication'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['anesthesia_method'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('surgeries', 'ID'),
            'medical_card_form_id' => Yii::t('surgeries', 'Medical Card Form ID'),
            'name' => Yii::t('surgeries', 'Name'),
            'datetime' => Yii::t('surgeries', 'Datetime'),
            'anesthesia_method' => Yii::t('surgeries', 'Anesthesia Method'),
            'complication' => Yii::t('surgeries', 'Complication'),
            'doctor_id' => Yii::t('surgeries', 'Doctor'),
        ];
    }

    public function getMedicalCardForm()
    {
        return $this->hasOne(MedicalCardForm::className(), ['id' => 'medical_card_form_id']);
    }

    public function getUser()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctors()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }
}
