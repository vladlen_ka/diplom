<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Anatomopathological;

/**
 * AnatomopathologicalSearch represents the model behind the search form about `app\models\Anatomopathological`.
 */
class AnatomopathologicalSearch extends Anatomopathological
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'number', 'clinical_patalogoanatomy_compare', 'difference_reason', 'death_reason_number', 'doctor', 'department_head'], 'integer'],
            [['adres', 'date', 'diagnosis_main', 'diagnosis_complication', 'diagnosis_concomitant', 'death_reason_text', 'death_illness', 'main_illness', 'other_notes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Anatomopathological::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number' => $this->number,
            'date' => $this->date,
            'clinical_patalogoanatomy_compare' => $this->clinical_patalogoanatomy_compare,
            'difference_reason' => $this->difference_reason,
            'death_reason_number' => $this->death_reason_number,
            'doctor' => $this->doctor,
            'department_head' => $this->department_head,
        ]);

        $query->andFilterWhere(['like', 'adres', $this->adres])
            ->andFilterWhere(['like', 'diagnosis_main', $this->diagnosis_main])
            ->andFilterWhere(['like', 'diagnosis_complication', $this->diagnosis_complication])
            ->andFilterWhere(['like', 'diagnosis_concomitant', $this->diagnosis_concomitant])
            ->andFilterWhere(['like', 'death_reason_text', $this->death_reason_text])
            ->andFilterWhere(['like', 'death_illness', $this->death_illness])
            ->andFilterWhere(['like', 'main_illness', $this->main_illness])
            ->andFilterWhere(['like', 'other_notes', $this->other_notes]);

        return $dataProvider;
    }
}
