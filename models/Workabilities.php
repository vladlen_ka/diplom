<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "workabilities".
 *
 * @property integer $id
 * @property string $value
 */
class Workabilities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'workabilities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('workabilities', 'ID'),
            'value' => Yii::t('workabilities', 'Value'),
        ];
    }

    public function getWorkabilities() 
    {
        return Workabilities::find()->indexBy('id')->all();
    }

    public function getWorkabilityById($id)
    {
        if ($id) {
            return Workabilities::find($id)->one()['value'];    
        } else {
            return NULL;
        }
        
    }
}
