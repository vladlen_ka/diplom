<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MedicalCardForm;

/**
 * MedicalCardFormSearch represents the model behind the search form about `app\models\MedicalCardForm`.
 */
class MedicalCardFormSearch extends MedicalCardForm
{

    public $hospitalisation_datetime_from;
    public $hospitalisation_datetime_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'patient_card_id', 'department_id',  'returns_time', 'bed_days', 'move_to_department_id', 'blood_type_id', 'rhesus_affilation', 'hospitalisation_variants_id', 'other_medical_services_malignant_formation_id', 'work_incapacity_certificate_id', 'workability_id', 'outcome_id'], 'integer'],
            [['medical_card_number', 'hospitalisation_datetime', 'closed_datetime', 'chamber', 'rw_examination', 'vil_examination', 'side_effects', 'hospitalisation_time', 'diagnosis_default', 'diagnosis_hospitalisation', 'diagnosis_clinical', 'diagnosis_clinical_date', 'diagnosis_clinical_doctor', 'diagnosis_final', 'diagnosis_final_main', 'diagnosis_final_main_complication', 'diagnosis_final_concomitant', 'surgeries_id_list', 'other_medical_services', 'conclusion_expertise', 'notes', 'doctor', 'department_head', 'departments.dept_name', 'departments.dept_id', 'user.id' , 'hospitalisation_datetime_to', 'hospitalisation_datetime_from'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function attributes()
    {
        // делаем поле зависимости доступным для поиска
        return array_merge(parent::attributes(), ['departments.dept_id', 'user.id']);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MedicalCardForm::find()
            ->joinWith(['departments'])
            ->joinWith(['user'])
            ->where(['patient_card_id' => $params['id']])
            ->orderBy(['id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->sort->attributes['doctor_full_name'] = [
            'asc' => ['user.last_name' => SORT_ASC, 'user.first_name' => SORT_ASC, 'user.middle_name' => SORT_ASC],
            'desc' => ['user.last_name' => SORT_DESC, 'user.first_name' => SORT_DESC, 'user.middle_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['departments.dept_name'] = [
            'asc' => ['departments.dept_name' => SORT_ASC],
            'desc' => ['departments.dept_name' => SORT_DESC],
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['medical_card_number' => $this->medical_card_number]);

        if($this->getAttribute('departments.dept_id')) {
            $query->andWhere(['department_id' => $this->getAttribute('departments.dept_id')]);
        }

        if($this->getAttribute('user.id')) {
            $query->andWhere(['doctor' => $this->getAttribute('user.id')]);
        }        

        // фильтр по дате госпитализации
        if($this->hospitalisation_datetime_to) {
            $query->andWhere('hospitalisation_datetime <= "' . date('y-m-d H:i:s', strtotime($this->hospitalisation_datetime_to)) . '"');
        }
        if($this->hospitalisation_datetime_from) {
            $query->andWhere('hospitalisation_datetime >= "' . date('Y-m-d H:i:s', strtotime($this->hospitalisation_datetime_from)) . '"');
        }

        return $dataProvider;
    }
}
