<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends UserIdentity
{

    public $fullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['fullName' ,'authAssignment.item_name', 'username' ,'role'], 'safe'],
        ];
    }

    public function attributes()
    {
        // делаем поле зависимости доступным для поиска
        return array_merge(parent::attributes(), ['authAssignment.item_name']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserIdentity::find();
        $query->joinWith(['authAssignment a'], true, 'LEFT JOIN');
        $query->joinWith(['authItem']);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 5],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'fullName' => [
                    'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC, 'middle_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC, 'middle_name' => SORT_DESC],
                    'label' => 'Full Name',
                    'default' => SORT_ASC
                ],
                'username',
                'role' => [
                    'asc' => ['authAssignment.item_name' => SORT_ASC],
                    'desc' => ['authAssignment.item_name' => SORT_DESC],
                    'label' => 'User Role',
                    'default' => SORT_ASC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'auth_assignment.item_name', $this->getAttribute('authAssignment.item_name')]);
        
        // фильтр по имени
        $query->andWhere('first_name LIKE "%' . $this->fullName . '%" ' .
            'OR last_name LIKE "%' . $this->fullName . '%"'.
            'OR middle_name LIKE "%' . $this->fullName . '%"'.
            'OR CONCAT_WS(" ",last_name, first_name, middle_name) LIKE "%'.$this->fullName.'%"'
        );

        return $dataProvider;
    }
}
