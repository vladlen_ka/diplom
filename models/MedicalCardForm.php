<?php

namespace app\models;

use Yii;
use app\models\Departments;
use app\models\PatientCard;
use app\models\UserIdentity;
use app\models\Workabilities;
use app\models\Outcomes;
class MedicalCardForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'medical_card_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_card_id', 'department_id', 'returns_time', 'bed_days', 'move_to_department_id', 'blood_type_id', 'rhesus_affilation', 'hospitalisation_variants_id', 'other_medical_services_malignant_formation_id', 'work_incapacity_certificate_id', 'workability_id', 'outcome_id'], 'integer'],

            [['medical_card_number'], 'unique'],

            //[['diagnosis_default', 'diagnosis_hospitalisation', 'diagnosis_clinical', 'diagnosis_final', 'diagnosis_final_main', 'diagnosis_final_main_complication', 'diagnosis_final_concomitant'], 'match', 'pattern' => '[A-Z][0-9]{2}-[0-9]{1,2}.*'],

            [['medical_card_number','doctor', 'department_head'], 'required'],

            [['hospitalisation_datetime', 'diagnosis_clinical_date'], 'safe'],

            [['side_effects', 'diagnosis_default', 'diagnosis_hospitalisation', 'diagnosis_clinical', 'diagnosis_final', 'diagnosis_final_main', 'diagnosis_final_main_complication', 'diagnosis_final_concomitant', 'surgeries_id_list', 'other_medical_services', 'conclusion_expertise', 'notes'], 'string'],

            [['chamber', 'hospitalisation_time'], 'string', 'max' => 10],

            [['rw_examination', 'vil_examination', 'doctor', 'department_head'], 'string', 'max' => 50],
            
            [['sent_by_clinic'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('patient_medical_form', 'ID'),
            'patient_card_id' => Yii::t('patient_medical_form', 'Patient Card ID'),
            'medical_card_number' => Yii::t('patient_medical_form', 'Medical Card Number'),
            'hospitalisation_datetime' => Yii::t('patient_medical_form', 'Hospitalisation Datetime'),
            'closed_datetime' => Yii::t('patient_medical_form', 'Closed Datetime'),
            'department_id' => Yii::t('patient_medical_form', 'Department'),
            'chamber' => Yii::t('patient_medical_form', 'Chamber'),
            'returns_time' => Yii::t('patient_medical_form', 'Returns Time'),
            'bed_days' => Yii::t('patient_medical_form', 'Bed Days'),
            'move_to_department_id' => Yii::t('patient_medical_form', 'Move To Department'),
            'blood_type_id' => Yii::t('patient_medical_form', 'Blood Type'),
            'rhesus_affilation' => Yii::t('patient_medical_form', 'Rhesus Affilation'),
            'rw_examination' => Yii::t('patient_medical_form', 'Rw Examination'),
            'vil_examination' => Yii::t('patient_medical_form', 'Vil Examination'),
            'side_effects' => Yii::t('patient_medical_form', 'Side Effects'),
            'sent_by_clinic' => Yii::t('patient_medical_form', 'Sent By'),
            'hospitalisation_variants_id' => Yii::t('patient_medical_form', 'Hospitalisation Variants'),
            'hospitalisation_time' => Yii::t('patient_medical_form', 'Hospitalisation Time'),
            'diagnosis_default' => Yii::t('patient_medical_form', 'Diagnosis Default'),
            'diagnosis_hospitalisation' => Yii::t('patient_medical_form', 'Diagnosis Hospitalisation'),
            'diagnosis_clinical' => Yii::t('patient_medical_form', 'Diagnosis Clinical'),
            'diagnosis_clinical_date' => Yii::t('patient_medical_form', 'Diagnosis Clinical Date'),
            'diagnosis_clinical_doctor' => Yii::t('patient_medical_form', 'Diagnosis Clinical Doctor'),
            'diagnosis_final' => Yii::t('patient_medical_form', 'Diagnosis Final'),
            'diagnosis_final_main' => Yii::t('patient_medical_form', 'Diagnosis Final Main'),
            'diagnosis_final_main_complication' => Yii::t('patient_medical_form', 'Diagnosis Final Main Complication'),
            'diagnosis_final_concomitant' => Yii::t('patient_medical_form', 'Diagnosis Final Concomitant'),
            'surgeries_id_list' => Yii::t('patient_medical_form', 'Surgeries Id List'),
            'other_medical_services' => Yii::t('patient_medical_form', 'Other Medical Services'),
            'other_medical_services_malignant_formation_id' => Yii::t('patient_medical_form', 'Other Medical Services Malignant Formation'),
            'work_incapacity_certificate_id' => Yii::t('patient_medical_form', 'Work Incapacity Certificate'),
            'workability_id' => Yii::t('patient_medical_form', 'Workability'),
            'conclusion_expertise' => Yii::t('patient_medical_form', 'Conclusion Expertise'),
            'outcome_id' => Yii::t('patient_medical_form', 'Outcome'),
            'notes' => Yii::t('patient_medical_form', 'Notes'),
            'doctor' => Yii::t('patient_medical_form', 'Responsible Doctor'),
            'department_head' => Yii::t('patient_medical_form', 'Department Head'),
        ];
    }

    public function getPatient() 
    {
        return $this->hasOne(PatientCard::ClassName(), ['patient_id', 'patient_card_id']);
    }

    public function getDepartments() 
    {
        return $this->hasOne(Departments::ClassName(), ['dept_id' => 'department_id']);
    }

    public function getMovedtodepartments() 
    {
        return $this->hasOne(Departments::ClassName(), ['dept_id' => 'move_to_department_id']);
    }

    public function getUser()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor']);
    }

    public function getDoctor()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor']);
    }

    public function getDoctors()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor']);
    }

    public function getDepartmentHead()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'department_head']);
    }

    public function getServiceMalignantFormations()
    {
        return $this->hasOne(ServiceMalignantFormations::className(), ['id' => 'other_medical_services_malignant_formation_id']);
    }

    public function getBloodTypeById($id) 
    {
        switch ($id) {
            case '1':
                return Yii::t('patient_medical_form','0 (I) - first type');
                break;
            case '2':
                return Yii::t('patient_medical_form','A (II) - second type');
                break;
            case '3':
                return Yii::t('patient_medical_form','B (III) - third type');
                break;
            case '4':
                return Yii::t('patient_medical_form','AB (IV) - fourth type');
                break;
            default:
                return NULL;
                break;
        }
    }

    public function getRhesusAffilationById($id) 
    {
        switch ($id) {
            case '0':
                return Yii::t('patient_medical_form','Rh(-) - negative');
                break;
            case '1':
                return Yii::t('patient_medical_form','Rh(+) - positive');
                break;
            default:
                return NULL;
                break;
        }
    }

    public function getWorkability()
    {
        return $this->hasOne(Workabilities::className(), ['id' => 'workability_id']);
    }

    public function getOutcome()
    {
        return $this->hasOne(Outcomes::className(), ['id' => 'outcome_id']);
    }


    public function getCardClosedTimeById($id) 
    {
        $res = MedicalCardForm::find()->select('closed_datetime')->where("id = $id")->asArray()->one();
        
        if ($res['closed_datetime'] != NULL && $res['closed_datetime'] != '0000-00-00 00:00:00') {
            return false;
        } 
        
        return true;
    }

    public function getDiagnosisFinalMainById($id) 
    {

       
        if ($id) {
            return MedicalCardForm::find()->where("id = $id")->one()['diagnosis_final_main'];    
        } else {
            return NULL;
        }
    }


    public function getDiagnosisFinalMainComplicationById($id) 
    {
        if ($id) {
            return MedicalCardForm::find()->where("id = $id")->one()['diagnosis_final_main_complication'];    
        } else {
            return NULL;
        }
    }

    public function getDiagnosisFinalMainConcomitantById($id) 
    {
        if ($id) {
            return MedicalCardForm::find()->where("id = $id")->one()['diagnosis_final_concomitant'];    
        } else {
            return NULL;
        }
    }

    public function getPatientLastCardById($id) 
    {
        $result = MedicalCardForm::find()->join('LEFT JOIN', 'patient_card', 'patient_card.patient_id = medical_card_form.patient_card_id')->where('patient_card.user_id = "' . $id . '" AND patient_vote IS NULL')->one();
        
        return $result;
    }
}
