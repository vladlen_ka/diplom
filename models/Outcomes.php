<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "outcomes".
 *
 * @property integer $id
 * @property string $value
 */
class Outcomes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outcomes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('outcomes', 'ID'),
            'value' => Yii::t('outcomes', 'Value'),
        ];
    }

    public function getOutcomes()
    {
        return Outcomes::find()->indexBy('id')->all();
    }

    public function getOutcomeById($id) 
    {
        if ($id) {
            return Outcomes::find($id)->one()['value'];    
        } else {
            return NULL;
        }

    }
}
