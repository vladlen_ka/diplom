<?php

namespace app\models;

use Yii;
use app\models\UserIdentity;
use app\models\MedicalCardForm;
/**
 * This is the model class for table "diary".
 *
 * @property integer $id
 * @property integer $medical_card_form_id
 * @property string $datetime_added
 * @property string $pulse
 * @property string $arterial_pressure
 * @property string $temprature
 * @property string $breathing
 * @property string $weight
 * @property string $drink_quantity
 * @property string $daily_pee
 * @property string $emptying
 * @property string $bath
 * @property string $comments
 * @property integer $doctor_id
 */
class Diary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['medical_card_form_id', 'doctor_id', 'pulse'], 'integer'],
            [['datetime_added'], 'safe'],
            [['comments'], 'string'],
            [['pulse', 'arterial_pressure', 'temprature', 'breathing', 'weight', 'drink_quantity', 'daily_pee', 'emptying', 'bath'], 'string', 'max' => 50],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('diary', 'ID'),
            'medical_card_form_id' => Yii::t('diary', 'Medical Card Form ID'),
            'datetime_added' => Yii::t('diary', 'Datetime Added'),
            'pulse' => Yii::t('diary', 'Pulse'),
            'arterial_pressure' => Yii::t('diary', 'Arterial Pressure'),
            'temprature' => Yii::t('diary', 'Temprature'),
            'breathing' => Yii::t('diary', 'Breathing'),
            'weight' => Yii::t('diary', 'Weight'),
            'drink_quantity' => Yii::t('diary', 'Drink Quantity'),
            'daily_pee' => Yii::t('diary', 'Daily Pee'),
            'emptying' => Yii::t('diary', 'Emptying'),
            'bath' => Yii::t('diary', 'Bath'),
            'comments' => Yii::t('diary', 'Comments'),
            'doctor_id' => Yii::t('diary', 'Doctor ID'),
        ];
    }

    public function getMedicalCardForm()
    {
        return $this->hasOne(MedicalCardForm::className(), ['id' => 'medical_card_form_id']);
    }

    public function getUser()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctors()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }
}
