<?php

namespace app\models;

use Yii;
use app\models\UserIdentity;
use app\models\MedicalCardForm;
/**
 * This is the model class for table "work_incapacity_lists".
 *
 * @property integer $id
 * @property integer $medical_card_form_id
 * @property string $number
 * @property string $datetime_from
 * @property string $datetime_to
 */
class WorkIncapacityLists extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'work_incapacity_lists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['medical_card_form_id'], 'integer'],
            [['number'], 'unique'],
            [['datetime_from', 'datetime_to'], 'safe'],
            [['number'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('work_incapacity_lists', 'ID'),
            'medical_card_form_id' => Yii::t('work_incapacity_lists', 'Medical Card Form ID'),
            'number' => Yii::t('work_incapacity_lists', 'Number'),
            'datetime_from' => Yii::t('work_incapacity_lists', 'Datetime From'),
            'datetime_to' => Yii::t('work_incapacity_lists', 'Datetime To'),
        ];
    }

    public function getMedicalCardForm()
    {
        return $this->hasOne(MedicalCardForm::className(), ['id' => 'medical_card_form_id']);
    }

    public function getUser()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctors()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }
}
