<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quality_indicator_weight".
 *
 * @property integer $id
 * @property string $name
 * @property integer $weight
 */
class QualityIndicatorWeight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quality_indicator_weight';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['weight'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('home', 'ID'),
            'name' => Yii::t('home', 'Name'),
            'weight' => Yii::t('home', 'Weight'),
        ];
    }
}
