<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "difference_reason".
 *
 * @property integer $id
 * @property string $value
 */
class DifferenceReason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'difference_reason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'value'], 'required'],
            [['id'], 'integer'],
            [['value'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('anatomopathological', 'ID'),
            'value' => Yii::t('anatomopathological', 'Value'),
        ];
    }

    public function getDiagnoses()
    {
        return DifferenceReason::find()->indexBy('id')->all();
    }

    public function getDiagnosisByid($id)
    {
        return DifferenceReason::find($id)->indexBy('id')->asArray()->one()['value'];
    }
}
