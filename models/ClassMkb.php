<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "class_mkb".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $parent_id
 * @property string $parent_code
 * @property integer $node_count
 */
class ClassMkb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_mkb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['parent_id', 'node_count'], 'integer'],
            [['name'], 'string', 'max' => 512],
            [['code', 'parent_code'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('class_mkb', 'ID'),
            'name' => Yii::t('class_mkb', 'Name'),
            'code' => Yii::t('class_mkb', 'Code'),
            'parent_id' => Yii::t('class_mkb', 'Parent ID'),
            'parent_code' => Yii::t('class_mkb', 'Parent Code'),
            'node_count' => Yii::t('class_mkb', 'Node Count'),
        ];
    }

    public function getDiagnoses() {
        $posts = ClassMkb::find()->select([new \yii\db\Expression("CONCAT(`code`, ' ', `name`) as diagnosis")])->where("type = 'diagnoses'  OR type = 'nosologies'")->orderBy(['code'=>SORT_ASC])->asArray()->all();
        $data = array();

        foreach ($posts as $post) {
            array_push($data, $post['diagnosis']);
        }
        
        return $data;
    }
}
