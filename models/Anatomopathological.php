<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "anatomopathological".
 *
 * @property integer $id
 * @property integer $number
 * @property string $adres
 * @property string $date
 * @property string $diagnosis_main
 * @property string $diagnosis_complication
 * @property string $diagnosis_concomitant
 * @property integer $clinical_patalogoanatomy_compare
 * @property integer $difference_reason
 * @property integer $death_reason_number
 * @property string $death_reason_text
 * @property string $death_illness
 * @property string $main_illness
 * @property string $other_notes
 * @property integer $doctor
 * @property integer $department_head
 */
class Anatomopathological extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'anatomopathological';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'clinical_patalogoanatomy_compare', 'difference_reason', 'death_reason_number', 'doctor', 'department_head'], 'integer'],
            [['adres', 'diagnosis_main', 'diagnosis_complication', 'diagnosis_concomitant', 'death_reason_text', 'death_illness', 'main_illness', 'other_notes'], 'string'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('patalogoanatomy', 'ID'),
            'number' => Yii::t('patalogoanatomy', 'Number'),
            'adres' => Yii::t('patalogoanatomy', 'Adres'),
            'date' => Yii::t('patalogoanatomy', 'Date'),
            'diagnosis_main' => Yii::t('patalogoanatomy', 'Diagnosis Main'),
            'diagnosis_complication' => Yii::t('patalogoanatomy', 'Diagnosis Complication'),
            'diagnosis_concomitant' => Yii::t('patalogoanatomy', 'Diagnosis Concomitant'),
            'clinical_patalogoanatomy_compare' => Yii::t('patalogoanatomy', 'Clinical Patalogoanatomy Compare'),
            'difference_reason' => Yii::t('patalogoanatomy', 'Difference Reason'),
            'death_reason_number' => Yii::t('patalogoanatomy', 'Death Reason Number'),
            'death_reason_text' => Yii::t('patalogoanatomy', 'Death Reason Text'),
            'death_illness' => Yii::t('patalogoanatomy', 'Death Illness'),
            'main_illness' => Yii::t('patalogoanatomy', 'Main Illness'),
            'other_notes' => Yii::t('patalogoanatomy', 'Other Notes'),
            'doctor' => Yii::t('patalogoanatomy', 'Doctor'),
            'department_head' => Yii::t('patalogoanatomy', 'Department Head'),
        ];
    }
}
