<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service_malignant_formations".
 *
 * @property integer $id
 * @property string $value
 */
class ServiceMalignantFormations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_malignant_formations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('service_malignant_formations', 'ID'),
            'value' => Yii::t('service_malignant_formations', 'Value'),
        ];
    }

    public function getValues() 
    {
        return ServiceMalignantFormations::find()->all();
    }
}
