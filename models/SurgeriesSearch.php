<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Surgeries;

/**
 * SurgeriesSearch represents the model behind the search form about `app\models\Surgeries`.
 */
class SurgeriesSearch extends Surgeries
{

    public $datetime_from;
    public $datetime_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'medical_card_form_id', 'doctor_id'], 'integer'],
            [['name', 'datetime', 'anesthesia_method', 'user.id', 'complication' , 'datetime_from' , 'datetime_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

     public function attributes()
    {
        // делаем поле зависимости доступным для поиска
        return array_merge(parent::attributes(), ['user.id']);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Surgeries::find()
            ->joinWith(['user'])
            ->where(['medical_card_form_id' => $params['id']])
            ->orderBy(['id'=>SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
        $this->load($params);

        $dataProvider->sort->attributes['doctor_full_name'] = [
            'asc' => ['user.last_name' => SORT_ASC, 'user.first_name' => SORT_ASC, 'user.middle_name' => SORT_ASC],
            'desc' => ['user.last_name' => SORT_DESC, 'user.first_name' => SORT_DESC, 'user.middle_name' => SORT_DESC],
        ];

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // фильтр по дате госпитализации
        if ($this->datetime_to) {
            $query->andWhere('datetime <= "' . date('y-m-d H:i:s', strtotime($this->datetime_to)) . '"');
        }
        if ($this->datetime_from) {
            $query->andWhere('datetime >= "' . date('Y-m-d H:i:s', strtotime($this->datetime_from)) . '"');
        }

        if ($this->name) {
            $query->andWhere('name LIKE "' . $this->name . "'");
        }

        if ($this->getAttribute('user.id')) {
            $query->andWhere(['doctor_id' => $this->getAttribute('user.id')]);
        } 

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'anesthesia_method', $this->anesthesia_method])
            ->andFilterWhere(['like', 'complication', $this->complication]);

        return $dataProvider;
    }
}
