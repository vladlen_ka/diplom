<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $last_name
 * @property string $first_name
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 */
class User extends \yii\db\ActiveRecord
{

    public $user_role;
    public $item_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name', 'middle_name', 'username', 'password', 'user_role'], 'required'],
            [['last_name', 'first_name', 'middle_name', 'username'], 'string', 'max' => 50],
            [['password', 'auth_key', 'access_token'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['authAssignment.item_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_name' => Yii::t('user', 'Last Name'),
            'first_name' =>  Yii::t('user', 'First Name'),
            'middle_name' =>  Yii::t('user', 'Middle Name'),
            'username' =>  Yii::t('user', 'Login'),
            'password' =>  Yii::t('user', 'Password'),
            'auth_key' =>  Yii::t('user', 'Auth Key'),
            'access_token' =>  Yii::t('user', 'Access Token'),
            'user_role' =>  Yii::t('user', 'User Permission'),
            'fullName' =>  Yii::t('user', 'Full Name'),
        ];
    }

}
