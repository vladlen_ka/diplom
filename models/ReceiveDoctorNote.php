<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "receive_doctor_note".
 *
 * @property integer $id
 * @property integer $medical_card_form_id
 * @property string $complaints
 * @property string $disease_anamnesis
 * @property string $life_anamnesis
 * @property string $patient_objective_condition
 * @property string $datetime_added
 */
class ReceiveDoctorNote extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receive_doctor_note';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['medical_card_form_id'], 'integer'],
            [['complaints', 'disease_anamnesis', 'life_anamnesis', 'patient_objective_condition'], 'string'],
            [['datetime_added', 'doctor_id'], 'safe'],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('receive_doctor_note', 'ID'),
            'medical_card_form_id' => Yii::t('patient_medical_from', 'Medical Card Form №'),
            'doctor_id' => Yii::t('patient_medical_form', 'Doctor'),
            'complaints' => Yii::t('receive_doctor_note', 'Complaints'),
            'disease_anamnesis' => Yii::t('receive_doctor_note', 'Disease Anamnesis'),
            'life_anamnesis' => Yii::t('receive_doctor_note', 'Life Anamnesis'),
            'patient_objective_condition' => Yii::t('receive_doctor_note', 'Patient Objective Condition'),
            'datetime_added' => Yii::t('receive_doctor_note', 'Datetime Added'),
        ];
    }

    public function getMedicalCardForm()
    {
        return $this->hasOne(MedicalCardForm::className(), ['id' => 'medical_card_form_id']);
    }

    public function getUser()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctor()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }

    public function getDoctors()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['id' => 'doctor_id']);
    }
}
