<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PatientCard;

/**
 * PatientCardSearch represents the model behind the search form about `app\models\PatientCard`.
 */
class PatientCardSearch extends PatientCard
{

    public $date_of_birth_from;
    public $date_of_birth_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['patient_id', 'gender_val'], 'integer'],
            [['first_name', 'last_name', 'date_of_birth_from', 'date_of_birth_to',  'middle_name', 'gender.gender_id', 'date_of_birth', 'photo', 'address', 'phone', 'phone_additional', 'place_of_work', 'job_position', 'pilgova_grupa', 'pilgova_cat', 'pilgova_series', 'pilogva_number', 'medical_card_id_list'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function attributes()
    {
        // делаем поле зависимости доступным для поиска
        return array_merge(parent::attributes(), ['gender.gender_id']);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PatientCard::find();
        $query->joinWith(['gender']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 10],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'first_name' => [
                    'asc' => ['first_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC],
                    'label' => 'First Name',
                    'default' => SORT_ASC
                ],
                'last_name' => [
                    'asc' => ['last_name' => SORT_ASC],
                    'desc' => ['last_name' => SORT_DESC],
                    'label' => 'Last Name',
                    'default' => SORT_ASC
                ],
                'middle_name' => [
                    'asc' => ['middle_name' => SORT_ASC],
                    'desc' => ['middle_name' => SORT_DESC],
                    'label' => 'Middle Name',
                    'default' => SORT_ASC
                ],
                'gend' => [
                    'asc' => ['gender.gender_id' => SORT_ASC],
                    'desc' => ['gender.gender_id' => SORT_DESC],
                    'label' => 'Gender',
                    'default' => SORT_ASC
                ],
                'date_of_birth',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'gender_val', $this->getAttribute('gender.gender_id')]);

        // фильтр по дате рождения
        if($this->date_of_birth_to) {
            $query->andWhere('date_of_birth <= "' . date('y-m-d', strtotime($this->date_of_birth_to)) . '"');
        }
        if($this->date_of_birth_from) {
            $query->andWhere('date_of_birth >= "' . date('Y-m-d', strtotime($this->date_of_birth_from)) . '"');
        }

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name]);

        return $dataProvider;
    }
}
