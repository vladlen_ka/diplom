<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WorkIncapacityLists;

/**
 * WorkIncapacityListsSearch represents the model behind the search form about `app\models\WorkIncapacityLists`.
 */
class WorkIncapacityListsSearch extends WorkIncapacityLists
{

    public $datetime_from_to;
    public $datetime_from_from;
    public $datetime_to_to;
    public $datetime_to_from;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'medical_card_form_id'], 'integer'],
            [['number', 'datetime_from', 'datetime_to', 'datetime_from_to', 'datetime_from_from', 'datetime_to_to', 'datetime_to_from'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkIncapacityLists::find()
            ->where(['medical_card_form_id' => $params['id']])
            ->orderBy(['id'=>SORT_DESC]);;
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // фильтр по дате госпитализации
        if ($this->datetime_from_to) {
            $query->andWhere('datetime_from <= "' . date('y-m-d H:i:s', strtotime($this->datetime_from_to)) . '"');
        }
        if ($this->datetime_from_from) {
            $query->andWhere('datetime_from >= "' . date('Y-m-d H:i:s', strtotime($this->datetime_from_from)) . '"');
        }

        // фильтр по дате госпитализации
        if ($this->datetime_to_to) {
            $query->andWhere('datetime_to <= "' . date('y-m-d H:i:s', strtotime($this->datetime_to_to)) . '"');
        }
        if ($this->datetime_to_from) {
            $query->andWhere('datetime_to >= "' . date('Y-m-d H:i:s', strtotime($this->datetime_to_from)) . '"');
        }

        $query->andFilterWhere(['like', 'number', $this->number]);

        return $dataProvider;
    }
}
