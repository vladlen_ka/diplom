<?php

namespace app\models;

use Yii;
use app\models\Gender;
use app\models\MedicalCardForm;
use app\models\UserIdentity;
/**
 * This is the model class for table "patient_card".
 *
 * @property integer $patient_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property integer $gender_val
 * @property string $date_of_birth
 * @property string $photo
 * @property string $address
 * @property string $phone
 * @property string $phone_additional
 * @property string $place_of_work
 * @property string $job_position
 * @property string $pilgova_grupa
 * @property string $pilgova_cat
 * @property string $pilgova_series
 * @property string $pilogva_number
 * @property string $medical_card_id_list
 */
class PatientCard extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patient_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'middle_name', 'date_of_birth', 'address', 'phone'], 'required'],
            [['gender_val'], 'integer'],
            [['date_of_birth'], 'safe'],
            //[['date_of_birth'], 'date'],
            [['photo','address'], 'string'],
            [['first_name', 'last_name', 'middle_name'], 'string', 'max' => 50],
            [['place_of_work', 'job_position', 'pilgova_grupa', 'pilgova_cat', 'pilgova_series', 'pilogva_number'], 'string', 'max' => 200],
            [['phone', 'phone_additional'], 'match', 'pattern' => '/\+38\([0-9]{3}\)[0-9]{3}\-[0-9]{4}/s', 'message' => Yii::t('patient_card', 'Invalid phone format')],
            [['image'], 'file','extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'patient_id' => Yii::t('patient_card', 'patient_id'),
            'first_name' => Yii::t('patient_card', 'First Name'),
            'last_name' => Yii::t('patient_card', 'Last Name'),
            'middle_name' => Yii::t('patient_card', 'Middle Name'),
            'gender_val' => Yii::t('patient_card', 'Gender Value'),
            'date_of_birth' => Yii::t('patient_card', 'Date Of Birth'),
            'photo' => Yii::t('patient_card', 'Photo'),
            'image' => Yii::t('patient_card', 'Photo'),
            'address' => Yii::t('patient_card', 'Address'),
            'phone' => Yii::t('patient_card', 'Phone'),
            'phone_additional' => Yii::t('patient_card', 'Phone Additional'),
            'place_of_work' => Yii::t('patient_card', 'Place Of Work'),
            'job_position' => Yii::t('patient_card', 'Job Position'),
            'pilgova_grupa' => Yii::t('patient_card', 'Pilgova Grupa'),
            'pilgova_cat' => Yii::t('patient_card', 'Pilgova Cat'),
            'pilgova_series' => Yii::t('patient_card', 'Pilgova Series'),
            'pilogva_number' => Yii::t('patient_card', 'Pilogva Number'),
        ];
    }

    public function getGender() {
        return $this->hasOne(Gender::className(), ['gender_id' => 'gender_val']);
    }

    public function upload($fileName)
    {
        if ($this->validate()) {
            $this->image->saveAs('images/patient_photo/' . $fileName);
            return true;
        } else {
            return false;
        }
    }

    public function getFullNameById($patient_id) {

        $user_data = PatientCard::findOne($patient_id);

        return $user_data->first_name . ' ' . $user_data->last_name . ' ' . $user_data->middle_name;
    } 

    public function getMedicalCardForm() 
    {
        return $this->hasMany(MedicalCardForm::ClassName(), ['patient_card_id' => 'patient__id']);
    }

    public function getUser()
    {
        return $this->hasOne(UserIdentity::ClassName(), ['user_id' => 'id']);
    }

    public function updatePatientFromUser($data = null) 
    {
        if ($data) {
            $patient_data = PatientCard::find()->where(['user_id' => $data->id])->one();

            $patient_data->last_name = $data->last_name;
            $patient_data->middle_name = $data->middle_name;
            $patient_data->first_name = $data->first_name;

            if($patient_data->save(false)) {
                return true;            
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getIdByUserId($id)
    {
        if ($id) {
            return PatientCard::find()->where("user_id = ".$id)->one()['patient_id'];    
        } else {
            return NULL;
        }
    } 
    public function getUserIdByCardId($patient_id) 
    {
        if ($patient_id) {
            return PatientCard::find()->where("patient_id = ".$patient_id)->one()['user_id'];    
        } else {
            return NULL;
        }
    }
}
