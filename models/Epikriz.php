<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "epikriz".
 *
 * @property integer $id
 * @property integer $medical_card_id
 * @property string $notes
 * @property string $blood
 * @property string $pee
 * @property string $liver
 * @property string $coprogram
 * @property string $blood_glukose
 * @property string $pee_amilase
 * @property string $pee_diastase
 * @property string $pee_glukose
 * @property string $healing
 * @property string $healing_and_recommendations
 * @property string $scabies_review
 * @property string $pediculosis
 * @property string $date_created
 */
class Epikriz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'epikriz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['medical_card_id', 'notes', 'blood', 'pee', 'liver', 'coprogram', 'blood_glukose', 'pee_amilase', 'pee_diastase', 'pee_glukose', 'healing', 'healing_and_recommendations', 'scabies_review', 'pediculosis', 'date_created'], 'required'],
            [['medical_card_id'], 'integer'],
            [['notes', 'blood', 'pee', 'liver', 'coprogram', 'blood_glukose', 'pee_amilase', 'pee_diastase', 'pee_glukose', 'healing', 'healing_and_recommendations', 'scabies_review', 'pediculosis'], 'string'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('epikriz', 'ID'),
            'medical_card_id' => Yii::t('epikriz', 'Medical Card ID'),
            'notes' => Yii::t('epikriz', 'Notes'),
            'blood' => Yii::t('epikriz', 'Blood'),
            'pee' => Yii::t('epikriz', 'Pee'),
            'liver' => Yii::t('epikriz', 'Liver'),
            'coprogram' => Yii::t('epikriz', 'Coprogram'),
            'blood_glukose' => Yii::t('epikriz', 'Blood Glukose'),
            'pee_amilase' => Yii::t('epikriz', 'Pee Amilase'),
            'pee_diastase' => Yii::t('epikriz', 'Pee Diastase'),
            'pee_glukose' => Yii::t('epikriz', 'Pee Glukose'),
            'healing' => Yii::t('epikriz', 'Healing'),
            'healing_and_recommendations' => Yii::t('epikriz', 'Healing And Recommendations'),
            'scabies_review' => Yii::t('epikriz', 'Scabies Review'),
            'pediculosis' => Yii::t('epikriz', 'Pediculosis'),
            'date_created' => Yii::t('epikriz', 'Date Created'),
        ];
    }
}
