<?php

namespace app\models;

use Yii;
use app\models\MedicalCardForm;
/**
 * This is the model class for table "departments".
 *
 * @property integer $id
 * @property string $name
 */
class Departments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'departments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dept_id' => Yii::t('patient_medical_form', 'Dept ID'),
            'name' => Yii::t('patient_medical_form', 'Name'),
        ];
    }
    
    public function getMedicalCardForm() 
    {
        return $this->hasMany(MedicalCardForm::ClassName(), ['department_id' => 'dept_id']);
    }

    public function getDepartments() 
    {
        return Departments::find()->indexBy('dept_id')->all();
    }

    public function getDepartmentById($id) 
    {
        if ($id) {
            return Departments::find($id)->one()['dept_name'];    
        } else {
            return NULL;
        }
    }
}
