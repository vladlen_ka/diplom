<?php

namespace app\models;

use Yii;
use yii\data\ArrayDataProvider;
use app\models\MedicalCardForm;
use app\models\ReceiveDoctorNote;
use app\models\Diary;
use app\models\Anatomopathological;
use app\models\Epikriz;

/*
1	Забезпечено первинний огляд. Термін 2 години з моменту надходження, для екстрених – негайно (30 хв.).		
2	Розпочато лікування (Створено запис у щоденнику). Термін 1 доба з моменту надхоження, для екстрених – негайно (1 год.).		
3	Встановити клінічний діагноз. Термін 3 доби з моменту надходження, для екстрених 2доби.		
4	Є щоденні записи у щоденнику.		
5	У всіх записах щоденника заповнені всі обов’язкові поля.		
6	Наявний епікриз/паталоанатом. листок.		
7	Сбір скарг		
8	Сбір анамнезу хвороби		
9	Сбір анамнезу життя		
10	Встановлення об’єктивного стану пацієнта		
11	Вказано відділення		
12	Ознайомлення дітей / батьків з правилами внутрішнього трудового розпорядку лікарні	(доступ пациент)	
13	Індекс задоволенності (доступ пациент)


hospitalisation_variants_id = срочно -1
За планом -2

*/

class Statistics
{
    public function getTableStatistic($query_params, $chart = false)
    {
        $where = 'move_to_department_id IS NULL AND closed_datetime IS NOT NULL AND closed_datetime != "0000-00-00 00:00:00"';

        if(isset($query_params['datetime_from']) && !empty($query_params['datetime_from'])) {
            $where .= ' AND closed_datetime >= "' . $query_params['datetime_from'] . '"';
        }

        if(isset($query_params['datetime_to']) && !empty($query_params['datetime_to'])) {
            $where .= ' AND closed_datetime <= "' . $query_params['datetime_to'] .'"';
        }

    	$medical_cards = MedicalCardForm::find()->where($where)->asArray()->all();

    	$result = array();

    	foreach ($medical_cards as $medical_card_key => $card) {

    		/*Пункт 1*/
    		$doctor_notes = ReceiveDoctorNote::find()->where("medical_card_form_id = " . $card['id'])->asArray()->all();

			if ($doctor_notes != NULL) {

	    		foreach ($doctor_notes as $doctor_note_key => $doctor_note) {

	    			$time_diff = $this->getTimeDifference($card['hospitalisation_datetime'], $doctor_note['datetime_added']);

	    			if ($card['hospitalisation_variants_id'] == 1) {
	    				$n1 = false;
	    				if ($time_diff[0] < '30' && $time_diff[1] == 'i') {
	    					$n1 = true;
	    					break;
	    				}
	    			} else {
	    				$n1 = false;
	    				if (($time_diff[1] == 'h' && $time_diff[0] < '2') || $time_diff[1] == 'i') {
	    					$n1 = true;
	    					break;
	    				}
	    			} 
	    		}

	    	} else {
	    		$n1 = null;
	    	}
    		/*Пункт 1*/

    		/*Пункт 2*/
    		$diary = Diary::find()->where("medical_card_form_id = " . $card['id'])->asArray()->all();
    		if ($diary != null) {
	    		foreach ($diary as $diary_key => $diary_value) {

	    			$time_diff = $this->getTimeDifference($card['hospitalisation_datetime'], $diary_value['datetime_added']);
		    			if ($card['hospitalisation_variants_id'] == 1) {
		    				$n2 = false;
		    				if (($time_diff[1] == 'h' && $time_diff[0] < '1') || $time_diff[1] == 'i') {
		    					$n2 = true;
		    					break;
		    				}
		    			} else {
		    				$n2 = false;
		    				if (($time_diff[1] == 'd' && $time_diff[0] < '1') || $time_diff[1] == 'i' || $time_diff[1] == 'h') {
		    					$n2 = true;
		    					break;
		    				}
		    			} 
	    		}
	    	} else {
	    		$n2 = null;
	    	}
    		/*Пункт 2*/

    		/*Пункт 3*/
    		if ($card['diagnosis_clinical'] != NULL && $card['diagnosis_clinical'] != '') {
    			$time_diff = $this->getTimeDifference($card['hospitalisation_datetime'], $card['diagnosis_clinical_date']);

	    			if ($card['hospitalisation_variants_id'] == 1) {
	    				$n3 = false;
	    				if (($time_diff[1] == 'd' && $time_diff[0] < '2') || $time_diff[1] == 'i' || $time_diff['h']) {
	    					$n3 = true;
	    					
	    				}
	    			} else {
	    				$n3 = false;
	    				if (($time_diff[1] == 'd' && $time_diff[0] < '3') || $time_diff[1] == 'i' || $time_diff[1] == 'h') {
	    					$n3 = true;
	    					
	    				}
	    			} 
    		} else {
    			$n3 = null;
    		}
    		/*Пункт 3*/

    		/*Пункт 4*/
    		$diary_notes = Diary::find()->where("medical_card_form_id = " . $card['id'] . " AND datetime_added <= '" . $card['closed_datetime'] . "' AND datetime_added >= '" . $card['hospitalisation_datetime'] . "'")->asArray()->orderBy('datetime_added ASC')->all();
    		if ($diary_notes != NULL) {
    			$last_date = '';

    			foreach ($diary_notes as $diary_note_key => $diary_note_value) {
    				$n4 = true;
    				
    				if ($diary_note_key == 0) {
    					$time_diff = $this->getTimeDifference($card['hospitalisation_datetime'], $diary_note_value['datetime_added']);
    				} elseif($diary_note_key == count($diary_notes) - 1) {
    					$time_diff = $this->getTimeDifference($diary_note_value['datetime_added'], $card['closed_datetime']);
    				} else {
    					$time_diff = $this->getTimeDifference($last_date, $diary_note_value['datetime_added']);
    				}
    				
    				if ($time_diff[1] == 'd' && $time_diff[0] >= '1') {

	    				$n4 = false;
	    				break;
	    			}

    				$last_date = $diary_note_value['datetime_added'];
    			}
    		} else {
    			$n4 = null;
    		}
    		/*Пункт 4*/
    		/*Пункт 5*/
$n5 = null;
    		foreach ($diary as $diary_value) {
    			$n5 = true;
    			if (in_array(null, $diary_value, true)) {
    				$n5 = false;
    				break;
				}	
    		}
    		/*Пункт 5*/
    		/*Пункт 6*/
    		$n6 = false;
    		if (Epikriz::find()->where('medical_card_id = ' . $card['id'])->asArray()->one() != NULL || Anatomopathological::find()->where('medical_card_id = ' .$card['id'])->asArray()->one() != NULL) {
    			$n6 = true;
    		}
    		/*Пункт 6*/

    		/*Пункт 7*/
    		if ($doctor_notes != null) {
    			foreach ($doctor_notes as $notes_key => $notes_value) {
    				$n7 = false;
    				if ($notes_value['complaints'] != null && $notes_value['complaints'] != '') {
    					$n7 = true;
    					break;
    				}
    			}
    		} else {
    			$n7 = null;
    		}
    		/*Пункт 7*/
    		/*Пункт 8*/
    		if ($doctor_notes != null) {
    			foreach ($doctor_notes as $notes_key => $notes_value) {
    				$n8 = false;
    				if ($notes_value['disease_anamnesis'] != null && $notes_value['disease_anamnesis'] != '') {
    					$n8 = true;
    					break;
    				}
    			}
    		} else {
    			$n8 = null;
    		}
    		/*Пункт 8*/
    		/*Пункт 9*/
    		if ($doctor_notes != null) {
    			foreach ($doctor_notes as $notes_key => $notes_value) {
    				$n9 = false;
    				if ($notes_value['life_anamnesis'] != null && $notes_value['life_anamnesis'] != '') {
    					$n9 = true;
    					break;
    				}
    			}
    		} else {
    			$n9 = null;
    		}
    		/*Пункт 9*/
    		/*Пункт 10*/
    		if ($doctor_notes != null) {
    			foreach ($doctor_notes as $notes_key => $notes_value) {
    				$n10 = false;
    				if ($notes_value['patient_objective_condition'] != null && $notes_value['patient_objective_condition'] != '') {
    					$n10 = true;
    					break;
    				}
    			}
    		} else {
    			$n10 = null;
    		}
    		/*Пункт 10*/
    		/*Пункт 11*/
    		$n11 = false;
    		if ($card['department_id'] != NULL) {
    			$n11 = true;
    		}
    		/*Пункт 11*/    		

            //Статистика по врачам-карточкам
    		if (UserIdentity::getDoctorById($card['doctor']) != NULL) {
                if(!$chart) {
		    	$result[UserIdentity::getDoctorById($card['doctor'])][$card['medical_card_number']] = array(
		    		'n1' => $n1,
		    		'n2' => $n2,
		    		'n3' => $n3,
		    		'n4' => $n4,
		    		'n5' => $n5,
		    		'n6' => $n6,
		    		'n7' => $n7,
		    		'n8' => $n8,
		    		'n9' => $n9,
		    		'n10' => $n10,
		    		'n11' => $n11,
		    	);
                } else {
                    $result[$card['closed_datetime']] = array(
                        'n1' => $n1,
                        'n2' => $n2,
                        'n3' => $n3,
                        'n4' => $n4,
                        'n5' => $n5,
                        'n6' => $n6,
                        'n7' => $n7,
                        'n8' => $n8,
                        'n9' => $n9,
                        'n10' => $n10,
                        'n11' => $n11,
                    );
                }
		    }
    	}
        return $result;
    }

    public function getDoctorStatistic($query_params) {
        $statistic = $this->getTableStatistic($query_params);

        $result = array();

        foreach ($statistic as $stat_key => $stat_value) {
            $counter = 0;
            foreach ($stat_value as $card_number => $stat_field) {
                $count = 0;
                foreach ($stat_field as $statistic_key => $statistic_value) {
                    if ($statistic_value === true) {
                        $count++;
                    }
                }
                $counter += $count;
            }
            //formula for doctor
            

            $stat_result = round(($counter/(count($stat_field)*count($stat_value)))*100, 2);

            $result[$stat_key] = $stat_result;
        }

        return $result;
    }   

    public function getCardStatistic($query_params) {
        $statistic = $this->getTableStatistic($query_params);

        $result = array();

        foreach ($statistic as $stat_key => $stat_value) {
            foreach ($stat_value as $card_number => $stat_field) {
                $count = 0;
                foreach ($stat_field as $statistic_key => $statistic_value) {
                    if ($statistic_value === true) {
                        $count++;
                    }
                }
                //formula for doctor_card
                $stat_result = round(($count/(count($stat_field)))*100, 2);

                $result[$stat_key][$card_number] = $stat_result;
            }
        }

        return $result;
    }

    public function getChartData($query_params) {
        $card_stat = $this->getTableStatistic($query_params, true);

        $result = array();
        $date_arr = array();

        foreach ($card_stat as $datetime_closed => $value) {
            array_push($date_arr, date('Y-m-d', strtotime($datetime_closed)));
        }

        $date_coincidence = array_count_values($date_arr);

        foreach ($card_stat as $datetime_closed => $stat_array) {
            $count = 0;
            foreach ($stat_array as $statistic_key => $statistic_value) {
                if ($statistic_value === true) {
                    $count++;
                }
            }
            

            $stat_result = round(($count/(count($stat_array)))*100, 2);

	    if (!isset($result[date('Y-m-d', strtotime($datetime_closed))]))
	    {
         	$result[date('Y-m-d', strtotime($datetime_closed))] = $stat_result;
            }	else {

		$result[date('Y-m-d', strtotime($datetime_closed))] += $stat_result;
	    }
        }

        $return = array();

        foreach ($result as $date => $stat) {
            if ($date_coincidence[$date] > 1) {
                $return[$date] = round($stat/($date_coincidence[$date]), 2);
            } else {
                $return[$date] = $stat;
            }
        }

        return $return;
    }


    protected function getTimeDifference($datetime_first, $datetime_second) 
    {
    	$date_diff = [ 0 => (floor((strtotime($datetime_second)  - strtotime($datetime_first)) / (60*60*24))), 1 => 'd'];
        if($date_diff[0] == '0') {
            $date_diff = [ 0 => (floor((strtotime($datetime_second)  - strtotime($datetime_first)) / (60*60))), 1 => 'h'];
            if($date_diff[0] == '0') {
                $date_diff = [ 0 => (floor((strtotime($datetime_second)  - strtotime($datetime_first)) / (60))), 1 => 'i'];
            } 
        } 

        return $date_diff;
    }
}
