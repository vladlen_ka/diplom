<?php

namespace app\models;

use app\models\AuthAssignment;
use app\models\PatientCard;
use Yii;

class UserIdentity extends User implements \yii\web\IdentityInterface
{



     public function rules()
    {
        return [
            [['last_name', 'first_name', 'middle_name', 'username', 'password', 'user_role'], 'required'],
            [['last_name', 'first_name', 'middle_name', 'username'], 'string', 'max' => 50],
            [['password', 'auth_key', 'access_token'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['authAssignment.item_name'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken($id)
    {
        return UserIdentity::find()->where("id = $id")->one()['access_token'];
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }

    public function getAuthAssignment() 
    {
        return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
    }

    public function getAuthItem() 
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'item_name'])
            ->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    public function getDoctorById($id = null)
    {


 $result = UserIdentity::find()->joinWith('authAssignment')->where("item_name = 'author'  AND id = '" .$id. "'")->asArray()->one();
    
        return $result['last_name'] . ' ' . $result['first_name'] . ' ' . $result['middle_name'];         
    }

    public function getFullName() 
    {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
    }   
    

    public function getPatientCard() 
    {
        return $this->hasOne(PatientCard::ClassName(), ['id' => 'user_id']);
    }   

    public function getDoctors() 
    {
        return UserIdentity::find()->joinWith('authAssignment')->where(['item_name' => 'author'])->all();
    }

    public function createUserFromPatientCard($data = null) 
    {
        if ($data) {
            
            $user = new UserIdentity;

            $user->last_name = $data->last_name;
            $user->middle_name = $data->middle_name;
            $user->first_name = $data->first_name;
            $user->username = 'patient_' . $data->last_name;
            $user->password = md5($user->username);
            $user->user_role = 'basic';

            if($user->save(false)) {

                $auth = Yii::$app->authManager;
                $userRole = $auth->getRole($user->user_role);
                $auth->assign($userRole, $user->getId());

                return $user->getId();            
            }else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updateUserFromPatientCard($data = null) 
    {
        if ($data) {
            $user_data = userIdentity::find()->where(['id' => $data->user_id])->one();

            $user_data->last_name = $data->last_name;
            $user_data->middle_name = $data->middle_name;
            $user_data->first_name = $data->first_name;

            if($user_data->save(false)) {
                return true;            
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
