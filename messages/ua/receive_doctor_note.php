<?php

return [
	'Receive Doctor Notes' => 'Записи лікаря приймального відділення',
	'Create Receive Doctor Note' => 'Створити запис лікаря приймального відділення',
	'Complaints' => 'Скарги',
	'Disease Anamnesis' => 'Анамнез хвороби',
	'Life Anamnesis' => 'Анамнез життя',
	'Patient Objective Condition' => 'Об\'єктивний стан пацієнта',
	'Datetime Added' => 'Дата додавання',
	'Receive Doctor Note' => 'Запис лікаря приймального відділення',
	'Update Receive Doctor Note:' => 'Оновити запис лікаря приймального відділення',
	'Responsible Doctor' => 'Лікар',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
];