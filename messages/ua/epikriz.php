<?php

return[
	'View Epikriz' => 'Епікриз',
	'Create Epikriz' => 'Стрворити Епікриз',
	'Epikrizs' => 'Епікриз',
	'Notes' => 'Запис',
	'Blood' => 'Загальний аналіз крові',
	'Pee' => 'Загальний аналіз сечі',
	'Liver' => 'Печінкові проби',
	'Coprogram' => 'Копрограма',
	'Blood Glukose' => 'Аналіз крові на глюкозу',
	'Pee Amilase' => 'Аналіз сечі на амілазу',
	'Pee Diastase' => 'Аналіз сечі на діастазу',
	'Pee Glukose' => 'Сеча на глюкозу (з добової кількості)',
	'Healing' => 'Лікування',
	'Healing And Recommendations' => 'Лікувальні та трудові рекомендації',
	'Scabies Review' => 'Огляд на коросту',
	'Pediculosis' => 'Педикульоз',
	'Date Created' => 'Дата',
	'' => '',
];