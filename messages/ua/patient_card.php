<?php

return [
	'Patient Cards' => 'Картки пацієнтів',
	'Create Patient Card' => 'Створити картку пацієнта',
	'Gender' => 'Стать',
	'Select gender' => 'Виберіть стать',
	'From' => 'Від',
	'To' => 'До',
	'Invalid phone format' => 'Неправильний формат номеру',
	'First Name' => 'Ім\'я',
	'Last Name' => 'Прізвище',
	'Middle Name' => 'По-батькові',
	'Gender Value' => 'Стать',
	'Date Of Birth' => 'Дата народження',
	'Photo' => 'Фотографія',
	'Address' => 'Адрес',
	'Phone' => 'Контактний телефон',
	'Phone Additional' => 'Додатковий телефон',
	'Place Of Work' => 'Місце роботи',
	'Job Position' => 'Посада',
	'Pilgova Grupa' => 'Пільгова група',
	'Pilgova Cat' => 'Пільгова категорія',
	'Pilgova Series' => 'Серія',
	'Pilogva Number' => 'Посідчення №',
	'Update PatientCard: ' => 'Редагувати картку:',
	'General info' => 'Загальна інформація',
	'Contacts' => 'Контактні дані',
	'Forms' => 'Мед. форми',
	'Departments Dept Name' => 'Відділення',
	'Doctor Full Name' => 'ПІБ лікаря',
];