<?php

return [
	'Diaries' => 'Щоденники',
	'Create Diary Note' => 'Створити запис у щоденнику',
	'Datetime Added' => 'Дата запису',
	'Pulse' => 'Пульс',
	'Arterial Pressure' => 'Артеріальний тиск',
	'Temprature' => 'Температура',
	'Breathing' => 'Дихання',
	'Weight' => 'Вага',
	'Drink Quantity' => 'Випито рідини',
	'Daily Pee' => 'Добова кількість сечі',
	'Emptying' => 'Випорожнення',
	'Bath' => 'Ванна',
	'Comments' => 'Коментарі',
	'Create Diary' => 'Створити запис',
	'Update Diary' => 'Оновити запис',
	'Diary Note: ' => 'Запис: ',
	'doctor' => 'Лікар',
];