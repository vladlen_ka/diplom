<?php
return [
	'Create Medical Card Form' => 'Створити мед. форму',
	'Input chamber' => 'Введіть палату',
	'Select department' => 'Виберіть відділення',
	'Select doctor' => 'Виберіть лікаря',
	'Update Medical Card Form: ' => 'Оновити мед. форму: ',
	'Medical card №' => 'Мед. форма №',
	'General info' => 'Загальна інформація',
	'Diagnoses' => 'Діагнози',
	'Surgeries' => 'Хірургічні операції',
	'Work Incapacity Lists' => 'Листки непрацездатності',
	'Conclusion' => 'Заключення',
	'Receive Doctor Notes' => 'Запис приймального відділення',
	'Diary' => 'Щоденник',
	'Workability' => 'Працезданість',
	'Outcome' => 'Результат лікування',
	'Responsible Doctor' => 'Лікуючий лікар',
	'Department Head' => 'Завідуючий відділенням',
	'Diagnosis Doctor' => 'Врач',
	'Medical Card Number' => 'Медична карта №',
	'Hospitalisation Datetime' => 'Госпіталізація',
	'Closed Datetime' => 'Виписка(смерть)',
	'Department' => 'Відділення',
	'Chamber' => 'Палата',
	'Returns Time' => 'В поточному році з приводу данної хвороби госпіталізований',
	'Move To Department' => 'Переведений(а) у відділення',
	'Blood Type' => 'Група крові',
	'Rhesus Affilation' => 'Резус приналежність',
	'Rw Examination' => 'Обстеження на RW',
	'Select RW Date' => 'Дата RW Обстеження',
	'Vil Examination' => 'Обстеження на ВІЛ',
	"Select Vil Date" => 'Дата обстеження на ВІЛ',
	'Side Effects' => 'Підвищена чутливість або непереносимість препарату',
	'Sent By' => 'Ким направлений хворий (найменування лікувального закладу)',
	'Hospitalisation Variants' => 'Госпіталізований(а) в стаціонар: ',
	'Hospitalisation Time' => 'Час госпіталізації',
	'Diagnosis Default' => 'Діагноз лікувального закладу , який направив хворого',
	'Diagnosis Hospitalisation' => 'Діагноз при госпіталізаціі',
	'Diagnosis Clinical' => 'Діагноз клінічний',
	'Diagnosis Clinical Date' => 'Дата встановлення',
	'Diagnosis Clinical Doctor' => 'Лікар',
	'Diagnosis Final' => 'Діагноз заключний клінічний',
	'Diagnosis Final Main' => 'Основний',
	'Diagnosis Final Main Complication' => 'Ускладнення основного',
	'Diagnosis Final Concomitant' => 'Супутній',
	'Other Medical Services' => 'Інші види лікування',
	'Other Medical Services Malignant Formation' => 'Для хворих на злоякісне утворення',
	'Conclusion Expertise' => 'Висновок для тих,хто поступає на експертизу',
	'Notes' => 'Особливі відмітки',
	'Choose Blood Type' => 'Виберіть групу крові',
	'Choose Rhesus' => 'Виберіть резус приналежність',
	'Urgent indications' => 'за терміновими показаннями',
	' hours after onset of the disease, an injury; routinely' => ' годин після захворювання, одержання травми; в плановому порядку',
	'Departments Dept Name' => 'Відділення',
	'Bed Days' => 'Ліжко-днів',
	'Card closed datetime : ' => 'Час закриття картки: ',
	'Additional' => 'Додатки',
	'Patient Cards' => 'Картки пацієнтів',
	'Doctor' => 'Лікар',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
];