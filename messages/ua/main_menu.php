<?php

return [
	'Medical Informational System' => 'Медична Інформаційна Система',
	'Patient Cards' => 'Картки пацієнтів',
	'Users' => 'Користувачі',
	'Logout' => 'Вийти', 
	'Dashboard' => 'Домашня сторінка',
	'About' => 'Про програму',
	'My profile' => 'Моя картка'
];