<?php

return [
	'Receive Doctor Notes' => 'Записи врача приемного отделения',
	'Create Receive Doctor Note' => 'Создать запись врача приемного отделения',
	'Complaints' => 'Жалобы',
	'Disease Anamnesis' => 'Анамнез болезни',
	'Life Anamnesis' => 'Анамнез жизни',
	'Patient Objective Condition' => 'Объективное состояние пациента',
	'Datetime Added' => 'Дата добавления',
	'Receive Doctor Note' => 'Запись врача приемного отделения',
	'Update Receive Doctor Note:' => 'Обновить запись врача приемного отделения',
	'Responsible Doctor' => 'Врач',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
];