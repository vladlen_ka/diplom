<?php

return [
	'Diaries' => 'Дневники',
	'Create Diary Note' => 'Создать запись в дневнике',
	'Datetime Added' => 'Дата записи',
	'Pulse' => 'Пульс',
	'Arterial Pressure' => 'Артериальное давление',
	'Temprature' => 'Температура',
	'Breathing' => 'Дыхание',
	'Weight' => 'Вес',
	'Drink Quantity' => 'Выпито жидкости',
	'Daily Pee' => 'Добова кількість сечі',
	'Emptying' => 'Випорожнення',
	'Bath' => 'Ванна',
	'Comments' => 'Комментарии',
	'Create Diary' => 'Создать запись',
	'Update Diary' => 'Редактировать запись',
	'Diary Note: ' => 'Запись: ',
	'doctor' => 'Врач',
];