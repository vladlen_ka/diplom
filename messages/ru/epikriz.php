<?php

return[
	'View Epikriz' => 'Просмотреть Эпикриз',
	'Create Epikriz' => 'Создать Эпикриз',
	'Epikrizs' => 'Эпикриз',
	'Notes' => 'Запись',
	'Blood' => 'Общий анализ крови',
	'Pee' => 'Общий анализ мочи',
	'Liver' => 'Печеночные пробы',
	'Coprogram' => 'Копрограмма',
	'Blood Glukose' => 'Анализ крови на глюкозу',
	'Pee Amilase' => 'Анализ мочи на амилазу',
	'Pee Diastase' => 'Анализ мочи на диастазу',
	'Pee Glukose' => 'Моча на глюкозу (из дневного кол-ва)',
	'Healing' => 'Лечение',
	'Healing And Recommendations' => 'Лечение и трудовые рекомендации',
	'Scabies Review' => 'Осмотр на чесотку',
	'Pediculosis' => 'Педикулез',
	'Date Created' => 'Дата',
	'' => '',
];