<?php

return [
	'Medical Informational System' => 'Медицинская информационная система',
	'Patient Cards' => 'Карточки пациентов',
	'Users' => 'Пользователи',
	'Logout' => 'Выйти',
	'Dashboard' => 'Домашняя страница',
	'About' => 'Про программу',
	'My profile' => 'Моя карточка',
];