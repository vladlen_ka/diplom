<?php

namespace app\controllers;

use Yii;
use app\models\Epikriz;
use app\models\EpikrizSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use app\models\PatientCard;
use app\models\MedicalCardForm;

/**
 * EpikrizController implements the CRUD actions for Epikriz model.
 */
class EpikrizController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Epikriz model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $patient_id, $medical_card_id)
    {

        if(Yii::$app->user->can('viewPost') || (PatientCard::getUserIdByCardId($patient_id) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {
            return $this->render('view', [
                'model' => $this->findModel($id),
                'patient' => PatientCard::findOne($patient_id),
                'medical_data' => MedicalCardForm::findOne($medical_card_id),
            ]);
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    /**
     * Creates a new  model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($patient_id, $medical_card_id)
    {
        if (\Yii::$app->user->can('createPost')) {
            $model = new Epikriz();
            $medical_data = MedicalCardForm::findOne($medical_card_id);

            $model->medical_card_id =  Yii::$app->request->get('medical_card_id');

            if ($model->load(Yii::$app->request->post()) && $medical_data->load(Yii::$app->request->post())) {
                $medical_data->closed_datetime = date('Y-m-d H:i:s');
                
            }

            if ($model->load(Yii::$app->request->post()) && $medical_data->load(Yii::$app->request->post()) && $medical_data->save() && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $patient_id, 'medical_card_id' => $medical_card_id]);
            } else {

                return $this->render('create', [
                    'model' => $model,
                    'patient' => PatientCard::findOne($patient_id),
                    'medical_data' => $medical_data,
                ]);
            }
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Finds the Epikriz model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Epikriz the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = Epikriz::findOne($id)) != NULL) {
            return $model;
        } else {
            return false;
        }
    }
}
