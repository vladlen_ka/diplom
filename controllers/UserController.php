<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserIdentity;
use app\models\UserSearch;
use app\models\PatientCard;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use app\models\AuthAssignment;
use app\models\MedicalCardForm;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (\Yii::$app->user->can('ViewUser')) {
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (\Yii::$app->user->can('viewUser')) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(\Yii::$app->user->can('createUser')) {
            $model = new UserIdentity();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                
                $model->password = md5($model->password);
                
                $model->save(false);

                $auth = Yii::$app->authManager;
                $userRole = $auth->getRole($model->user_role);
                $auth->assign($userRole, $model->getId());

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403 ,'Forbidden');
            
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('updateUser')) {
            $model = $this->findModel($id);

            $old_password = $model->password;

            $roles = $model->authAssignment;

            if ($model->load(Yii::$app->request->post()) && $roles->load(Yii::$app->request->post()) && $model->validate()) {

                if ($old_password != $model->password) {
                    $model->password = md5($model->password);
                }

                $model->save(false);
                $roles->save(false);
                    
                PatientCard::updatePatientFromUser($model);

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'roles' => $roles,
                ]);
            } 
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('deleteUser'))  {

            if ($auth_asssignment = AuthAssignment::find()->where(['user_id' => $id])->one()) {
                $auth_asssignment->delete();
            }

            if ($patient_card = PatientCard::find()->where(['user_id' => $id])->one()) {

                if ($medical_card_form = MedicalCardForm::find()->where(['patient_card_id' => $patient_card->patient_id])->all()) {
                    foreach ($medical_card_form as $med_form) {
                        $med_form->delete();
                    }
                }

                $patient_card->delete();
            }

            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserIdentity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
