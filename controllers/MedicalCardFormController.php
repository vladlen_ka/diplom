<?php

namespace app\controllers;

use Yii;
use app\models\MedicalCardForm;
use app\models\MedicalCardFormSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;

use app\models\PatientCard;
use app\components\Common;
use app\models\Epikriz;
use app\models\Diary;
use app\models\ReceiveDoctorNote;
use app\models\Surgeries;
use app\models\WorkIncapacityLists;
use app\models\Anatomopathological;



use kartik\mpdf\Pdf;
/**
 * MedicalCardFormController implements the CRUD actions for MedicalCardForm model.
 */
class MedicalCardFormController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MedicalCardForm models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('viewPost') || (PatientCard::getUserIdByCardId(Yii::$app->request->get('id')) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {

            $searchModel = new MedicalCardFormSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->renderPartial('acccess_denied');
        }
    }

    /**
     * Displays a single MedicalCardForm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $patient_id)
    {
        if (Yii::$app->user->can('viewPost') || (PatientCard::getUserIdByCardId($patient_id)== Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {

            $model = $this->findModel($id);

            if($model->hospitalisation_datetime != NULL && $model->closed_datetime == NULL) {
                $model->bed_days = round(floor(strtotime(date('Y-m-d H:i:s')) - strtotime($model->hospitalisation_datetime)) / (60 * 60 * 24),2);
            } elseif($model->hospitalisation_datetime != NULL && $model->closed_datetime != NULL) {
                $model->bed_days = round(floor(strtotime($model->closed_datetime) - strtotime($model->hospitalisation_datetime)) / (60 * 60 * 24),2);
            } else {
                $model->bed_days = NULL;
            }

            if ($model->hospitalisation_variants_id == 1) {
                $model->hospitalisation_variants_id = Yii::t('patient_medical_form', 'Urgent indications');
            } elseif($model->hospitalisation_variants_id == 2) {
                $model->hospitalisation_variants_id = $model->hospitalisation_time .  Yii::t('patient_medical_form', ' hours after onset of the disease, an injury; routinely');
            } else {
                $model->hospitalisation_variants_id = NULL;
            }
        

            $model->blood_type_id = Common::transelement(MedicalCardForm::getBloodTypeById($model->blood_type_id));
            $model->rhesus_affilation = Common::transelement(MedicalCardForm::getRhesusAffilationById($model->rhesus_affilation));

            return $this->render('view', [
                'model' => $model,
                'patient' => PatientCard::findOne($patient_id),
                'surgeries' => Yii::$app->runAction('surgeries/index'),
                'receive_doctor_note' => Yii::$app->runAction('receive-doctor-note/index'),
                'work_incapacity_lists' => Yii::$app->runAction('work-incapacity-lists/index'),
                'diary' => Yii::$app->runAction('diary/index'),
                'epikriz' => $this->findEpikriz($id),
                'anatomopathological' => $this->findAnatomopathological($id),
            ]);
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    /**
     * Creates a new MedicalCardForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($patient_id = null)
    {
        if ($patient_id) {
            if (Yii::$app->user->can('createPost')) {
                $model = new MedicalCardForm();

                $model->patient_card_id = $patient_id;
                
                if ($model->load(Yii::$app->request->post())) {
                    if ($model->move_to_department_id != NULL) {
                        $model->closed_datetime = date('Y-m-d H:i:s');
                    }
                    if ($model->diagnosis_clinical != NULL && $model->diagnosis_clinical != '') {
                        $model->diagnosis_clinical_date = date('Y-m-d H:i:s');
                    }
                }
                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $patient_id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                        'patient' => PatientCard::findOne($patient_id),
                    ]);
                }
            } else {
                throw new HttpException(403, 'Forbidden');
            }
        }  else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Updates an existing MedicalCardForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id,$patient_id = null)
    {
        if ($patient_id) {
            if(Yii::$app->user->can('updatePost')) {
                $model = $this->findModel($id);

                $model->patient_card_id = $patient_id;

                if ($model->load(Yii::$app->request->post())) {
                    if ($model->move_to_department_id != NULL) {
                        $model->closed_datetime = date('Y-m-d H:i:s');
                    }
                }

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $patient_id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'patient' => PatientCard::findOne($patient_id), 
                    ]);
                }
            } else {
                throw new HttpException(403, 'Forbidden');
            }
        }  else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Deletes an existing MedicalCardForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $patient_id)
    {
        if(Yii::$app->user->can('deletePost')) {
            
            //Епикриз
            if ($epikriz = Epikriz::find()->where(['medical_card_id' => $id])->one()) {            
                $epikriz->delete();
            }

            //Записи в дневнике
            if ($diary = Diary::find()->where(['medical_card_form_id' => $id])->all()) {
                foreach ($diary as $diar) {
                    $diar->delete();
                }
            }

            //Хирург операции
            if ($surgeries = Surgeries::find()->where(['medical_card_form_id' => $id])->all()) {
                foreach ($surgeries as $surgery) {
                    $surgery->delete();
                }
            }

            //Листки непрацездатності
            if ($work_incapacity_lists = WorkIncapacityLists::find()->where(['medical_card_form_id' => $id])->all()) {
                foreach ($work_incapacity_lists as $work_incapacity_list) {
                    $work_incapacity_list->delete();
                }
            }

            //Записи врача приемного
            if ($receive_doctor_notes = ReceiveDoctorNote::find()->where(['medical_card_form_id' => $id])->all()) {
                foreach ($receive_doctor_notes as $receive_doctor_note) {
                    $receive_doctor_note->delete();
                }
            }

            //Мед. форма
            $this->findModel($id)->delete();
            return $this->redirect(['patient-card/view', 'id' => $patient_id]);
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    public function actionOpenCard($id, $patient_id)
    {
        if(Yii::$app->user->can('updatePost')) {
            Yii::$app->db->createCommand()
                    ->update('medical_card_form', ['closed_datetime' => NULL], 'id = "' . $id . '"')
                    ->execute();

            return $this->redirect(['patient-card/view', 'id' => $patient_id]);           
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    public function actionCloseCard($id, $patient_id)
    {
        if(Yii::$app->user->can('updatePost')) {
            Yii::$app->db->createCommand()
                    ->update('medical_card_form', ['closed_datetime' => date('Y-m-d H:i:s')], 'id = "' . $id . '"')
                    ->execute();

            return $this->redirect(['patient-card/view', 'id' => $patient_id]);           
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    public function actionPdf($id, $patient_id) {
        if(Yii::$app->user->can('viewPost')) {
            $pdf = Yii::$app->pdf;
            
            $model = $this->findModel($id);

            if($model->hospitalisation_datetime != NULL && $model->closed_datetime == NULL) {
                $model->bed_days = round(floor(strtotime(date('Y-m-d H:i:s')) - strtotime($model->hospitalisation_datetime)) / (60 * 60 * 24),2);
            } elseif($model->hospitalisation_datetime != NULL && $model->closed_datetime != NULL) {
                $model->bed_days = round(floor(strtotime($model->closed_datetime) - strtotime($model->hospitalisation_datetime)) / (60 * 60 * 24),2);
            } else {
                $model->bed_days = NULL;
            }

            if ($model->hospitalisation_variants_id == 1) {
                $model->hospitalisation_variants_id = Yii::t('patient_medical_form', 'Urgent indications');
            } elseif($model->hospitalisation_variants_id == 2) {
                $model->hospitalisation_variants_id = $model->hospitalisation_time .  Yii::t('patient_medical_form', ' hours after onset of the disease, an injury; routinely');
            } else {
                $model->hospitalisation_variants_id = NULL;
            }
            


            $model->blood_type_id = Common::transelement(MedicalCardForm::getBloodTypeById($model->blood_type_id));
            $model->rhesus_affilation = Common::transelement(MedicalCardForm::getRhesusAffilationById($model->rhesus_affilation));

            $patient =PatientCard::findOne($patient_id);

            if($patient->date_of_birth)  {
                $patient->date_of_birth = $this->getTimeDifference($patient->date_of_birth);
                
            }

            $pdf->content = $this->renderPartial('form_pdf', [
                'model' => $model,
                'patient' => $patient,
                'surgeries' => Yii::$app->runAction('surgeries/index'),
                'receive_doctor_note' => Yii::$app->runAction('receive-doctor-note/index'),
                'work_incapacity_lists' => Yii::$app->runAction('work-incapacity-lists/index'),
                'diary' => Yii::$app->runAction('diary/index'),
                'epikriz' => $this->findEpikriz($id),
                'anatomopathological' => $this->findAnatomopathological($id),
            ]);
            return $pdf->render();
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    /**
     * Finds the MedicalCardForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MedicalCardForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MedicalCardForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findEpikriz($id)
    {
        if (($model = Epikriz::find()->where("medical_card_id = $id")->one()) !== null) {
            return $model;
        } else {
           return false;
        }
    }

    protected function findAnatomopathological($id)
    {
        if (($model = Anatomopathological::find()->where("medical_card_id = $id")->one()) !== null) {
            return $model;
        } else {
           return false;
        }
    }

    protected function getTimeDifference($datetime_first) 
    {
        $date = date('Y-m-d H:i:s');
        $age = date('Y') - date('Y', strtotime($datetime_first));
        if($age == '0') {
            $age = date('m') - date('m', strtotime($datetime_first));
            if ($age == '0') {
                $age =  (floor((strtotime($date)  - strtotime($datetime_first)) / (60*60*24)));
                if($age == '0') {
                    $age = (floor((strtotime($date)  - strtotime($datetime_first)) / (60*60)));
                    if($age == '0') {
                        $age = (floor((strtotime($date)  - strtotime($datetime_first)) / (60)));
                        $age_val = Yii::t('patient_medical_form', 'minutes');
                    } else {
                        $age_val = Yii::t('patient_medical_form', 'hours');
                    }
                } else {
                    $age_val = Yii::t('patient_medical_form', 'days');
                }
            } else {
                $age_val = Yii::t('patient_medical_form', 'months');
            }
        } else {
            $age_val = Yii::t('patient_medical_form', 'years');
        }
        return $age . ' ' . $age_val;
    }

}
