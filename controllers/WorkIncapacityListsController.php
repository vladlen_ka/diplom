<?php

namespace app\controllers;

use Yii;
use app\models\WorkIncapacityLists;
use app\models\WorkIncapacityListsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use app\models\PatientCard;
use app\models\MedicalCardForm;

/**
 * WorkIncapacityListsController implements the CRUD actions for WorkIncapacityLists model.
 */
class WorkIncapacityListsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WorkIncapacityLists models.
     * @return mixed
     */
    public function actionIndex($patient_id = null)
    {

        if (\Yii::$app->user->can('viewPost') || (PatientCard::getUserIdByCardId(Yii::$app->request->get('patient_id')) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {
            $searchModel = new WorkIncapacityListsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return 'Access Denied!';
        }
    }

    /**
     * Displays a single Surgeries model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $patient_id, $medical_card_id)
    {

        if (\Yii::$app->user->can('viewPost') || (PatientCard::getUserIdByCardId($patient_id) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {
            return $this->render('view', [
                'model' => $this->findModel($id),
                'patient' => PatientCard::findOne($patient_id),
                'medical_data' => MedicalCardForm::findOne($medical_card_id),
            ]);
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Creates a new WorkIncapacityLists model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($patient_id, $medical_card_id)
    {
        if (\Yii::$app->user->can('createPost')) {
            $model = new WorkIncapacityLists();

            $model->medical_card_form_id =  Yii::$app->request->get('medical_card_id');

            if ($model->load(Yii::$app->request->post()) && $model->save() && $model->validate()) {
                return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $patient_id, 'medical_card_id' => $medical_card_id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'patient' => PatientCard::findOne($patient_id),
                    'medical_data' => MedicalCardForm::findOne($medical_card_id),
                ]);
            }
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Updates an existing Surgeries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $patient_id, $medical_card_id)
    {
        if (\Yii::$app->user->can('updatePost')) {
            $model = $this->findModel($id);

            $model->medical_card_form_id =  Yii::$app->request->get('medical_card_id');

            if ($model->load(Yii::$app->request->post()) && $model->save() && $model->validate()) {
                return $this->redirect(['view', 'id' => $model->id, 'patient_id' => $patient_id, 'medical_card_id' => $medical_card_id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'patient' => PatientCard::findOne($patient_id),
                    'medical_data' => MedicalCardForm::findOne($medical_card_id),
                ]);
            }
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

   /**
     * Deletes an existing Surgeries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id,$patient_id,$medical_card_id)
    {
        if (\Yii::$app->user->can('deletePost')) {
            $this->findModel($id)->delete();

            return $this->redirect(['medical-card-form/view', 'id' => $medical_card_id, 'patient_id' => $patient_id]);
        } else {
            throw new HttpException(403 ,'Forbidden');
        }
    }

    /**
     * Finds the WorkIncapacityLists model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorkIncapacityLists the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WorkIncapacityLists::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
