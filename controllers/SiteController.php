<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Statistics;
use app\models\MedicalCardForm;
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   
	 if (Yii::$app->user->isGuest) {
            	$model = new LoginForm();
        	if ($model->load(Yii::$app->request->post()) && $model->login()) {
		    return $this->goBack();
		}

		$this->layout = 'login';
		if (Yii::$app->session->get('kicked_reason') != NULL) {
		    $kicked_reason = Yii::$app->session->get('kicked_reason');
		    Yii::$app->session->set('kicked_reason', NULL);
		} else {
		    $kicked_reason = NULL;
		}

		return $this->render('login', [
		    'model' => $model,
		    'kicked_reason' => $kicked_reason,
		]);
        }
        if (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin'])) {
            $statistics = new Statistics;

            $query_params = Yii::$app->request->queryParams;
            $stat = $statistics->getTableStatistic($query_params);

            $doctor_stat = $statistics->getDoctorStatistic($query_params);
            $doctor_card_stat = $statistics->getCardStatistic($query_params);
            $chart_data = $statistics->getChartData($query_params);

            return $this->render('statistics', [
                'model' => $stat,
                'doctor_stat' => $doctor_stat,
                'doctor_card_stat' => $doctor_card_stat,
                'chart_data' => $chart_data,
            ]);
        } elseif (isset(\Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['basic'])) {
            $model = MedicalCardForm::getPatientLastCardById(Yii::$app->user->identity->id);
            if($model) {
                if ($model->load(Yii::$app->request->post())) {

                    $vote = Yii::$app->request->post('MedicalCardForm')['patient_vote'];

                    $command = Yii::$app->getDb()->createCommand("
                        UPDATE medical_card_form SET 
                        patient_vote = '".$vote."'
                        WHERE id = '".$model->id."'
                        ");
                    if($command->execute()) {
                        return $this->refresh();
                    } else {
                        return $this->render('vote', [
                            'model' => $model,
                        ]);
                    }
                }
            }
            return $this->render('vote', [
                'model' => $model,
            ]);
        } else {
            return $this->render('index');
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $this->layout = 'login';
        if (Yii::$app->session->get('kicked_reason') != NULL) {
            $kicked_reason = Yii::$app->session->get('kicked_reason');
            Yii::$app->session->set('kicked_reason', NULL);
        } else {
            $kicked_reason = NULL;
        }

        return $this->render('login', [
            'model' => $model,
            'kicked_reason' => $kicked_reason,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
