<?php

namespace app\controllers;

use Yii;

use app\models\PatientCard;
use app\models\PatientCardSearch;
use app\models\MedicalCardForm;
use app\models\UserIdentity;
use app\models\AuthAssignment;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\web\UploadedFile;

use yii\filters\VerbFilter;
use app\models\Epikriz;
use app\models\Diary;
use app\models\ReceiveDoctorNote;
use app\models\Surgeries;
use app\models\WorkIncapacityLists;
/**
 * PatientCardController implements the CRUD actions for PatientCard model.
 */
class PatientCardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PatientCard models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        if(\Yii::$app->user->can('viewPatient')) {
            $searchModel = new PatientCardSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    /**
     * Displays a single PatientCard model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) 
    {
        
        if(\Yii::$app->user->can('viewPatient') || (PatientCard::getUserIdByCardId($id) == Yii::$app->user->identity->id && Yii::$app->user->can('viewPatientProfile'))) {
            return $this->render('view', [
                'model' => $this->findModel($id),
                'medical_card' => Yii::$app->runAction('medical-card-form/index'),
            ]);
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    /**
     * Creates a new PatientCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        if(\Yii::$app->user->can('createPatient')) {
            $model = new PatientCard();

            if ($model->load(Yii::$app->request->post())) {

                $model->image = UploadedFile::getInstance($model, 'image');

                $fileName = '';

                if ($model->image) {
                    $rnd = md5(rand(0,9999));  // generate random number between 0-9999
                    $fileName = "{$rnd}-{$model->image}";  // random number + file name
                    $model->photo = '/images/patient_photo/'.$fileName;
                }

                $model->user_id = UserIdentity::createUserFromPatientCard($model);

                if ($model->save() && $model->user_id) {
                    if ($model->image) {
                        $model->upload($fileName);
                    }
                    return $this->redirect(['view', 'id' => $model->patient_id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    /**
     * Updates an existing PatientCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(\Yii::$app->user->can('updatePatient')) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {

                $model->image = UploadedFile::getInstance($model, 'image');

                if ($model->image) {
                    $rnd = md5(rand(0,9999));  // generate random number between 0-9999
                    $fileName = "{$rnd}-{$model->image}";  // random number + file name
                    $model->photo = '/images/patient_photo/'.$fileName;
                }

                UserIdentity::updateUserFromPatientCard($model);

                if ($model->save()) {
                    if ($model->image) {
                        $model->upload($fileName);
                    }
                    return $this->redirect(['view', 'id' => $model->patient_id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new HttpException(403, 'Forbidden');
        }

    }

    /**
     * Deletes an existing PatientCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(\Yii::$app->user->can('deletePatient')) {
            $patient_data = $this->findModel($id);
            if ($auth_asssignment = AuthAssignment::find()->where(['user_id' => $patient_data->user_id])->one()) {
                $auth_asssignment->delete();
            }
            if ($user_identity = UserIdentity::find()->where(['id' => $patient_data->user_id])->one()) {
                $user_identity->delete();
            }
            if ($medical_card_form = MedicalCardForm::find()->where(['patient_card_id' => $patient_data->patient_id])->all()) {
                foreach ($medical_card_form as $med_form) {
                    $medical_card_id = $med_form->id;
                    //Епикриз
                    if ($epikriz = Epikriz::find()->where(['medical_card_id' => $medical_card_id])->one()) {            
                        $epikriz->delete();
                    }

                    //Записи в дневнике
                    if ($diary = Diary::find()->where(['medical_card_form_id' => $medical_card_id])->all()) {
                        foreach ($diary as $diar) {
                            $diar->delete();
                        }
                    }

                    //Хирург операции
                    if ($surgeries = Surgeries::find()->where(['medical_card_form_id' => $medical_card_id])->all()) {
                        foreach ($surgeries as $surgery) {
                            $surgery->delete();
                        }
                    }

                    //Листки непрацездатності
                    if ($work_incapacity_lists = WorkIncapacityLists::find()->where(['medical_card_form_id' => $medical_card_id])->all()) {
                        foreach ($work_incapacity_lists as $work_incapacity_list) {
                            $work_incapacity_list->delete();
                        }
                    }

                    //Записи врача приемного
                    if ($receive_doctor_notes = ReceiveDoctorNote::find()->where(['medical_card_form_id' => $medical_card_id])->all()) {
                        foreach ($receive_doctor_notes as $receive_doctor_note) {
                            $receive_doctor_note->delete();
                        }
                    }
                    $med_form->delete();
                }
            }

            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new HttpException(403, 'Forbidden');
        }
    }

    /**
     * Finds the PatientCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PatientCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PatientCard::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
