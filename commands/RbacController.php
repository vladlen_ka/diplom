<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use \app\rbac\UserGroupRule;
use \app\rbac\AuthorRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        /*
        *   Пациент
        *   Роль basic - Пациент
        *   Роль author - может создавать записи в больничных формах
        *   Роль admin - абсолютные права доступа
        */

        // добавляем разрешение "viewPatient"
        $viewPatient = $auth->createPermission('viewPatient');
        $viewPatient->description = 'View a patient';
        $auth->add($viewPatient);

        // добавляем разрешение "createPatient"
        $createPatient = $auth->createPermission('createPatient');
        $createPatient->description = 'Create a patient';
        $auth->add($createPatient);

        // добавляем разрешение "updatePatient"
        $updatePatient = $auth->createPermission('updatePatient');
        $updatePatient->description = 'Update patient';
        $auth->add($updatePatient);

        // "deletePatient"
        $deletePatient = $auth->createPermission('deletePatient');
        $deletePatient->description = 'Delete patient';
        $auth->add($deletePatient);

        /*
        *   Пользователи системы
        */

        // добавляем разрешение "createUser"
        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create a user';
        $auth->add($createUser);

        // добавляем разрешение "updateUser"
        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update a user';
        $auth->add($updateUser);

        // добавляем разрешение "deleteUser"
        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete a user';
        $auth->add($deleteUser);

        // "viewUser"
        $viewUser = $auth->createPermission('viewUser');
        $viewUser->description = 'Watch user info';
        $auth->add($viewUser);


        /**
        *   Больничные формы
        */
         // добавляем разрешение "createPost"
        $createPost = $auth->createPermission('createPost');
        $createPost->description = 'Create a post';
        $auth->add($createPost);

        // добавляем разрешение "updatePost"
        $updatePost = $auth->createPermission('updatePost');
        $updatePost->description = 'Update a post';
        $auth->add($updatePost);

        // добавляем разрешение "deletePost"
        $deletePost = $auth->createPermission('deletePost');
        $deletePost->description = 'Delete a post';
        $auth->add($deletePost);

        // "viewPost"
        $viewPost = $auth->createPermission('viewPost');
        $viewPost->description = 'Watch post info';
        $auth->add($viewPost);
    


// add the rule
        $rule = new AuthorRule;
        $auth->add($rule);
        /*Назначение прав*/

        // добавляем роль "basic" 
        $basic = $auth->createRole('basic');
        $basic->ruleName = $rule->name;
        $auth->add($basic);
        $auth->addChild($basic, $viewPatient);

        //Добавляем роль "author"
        $author = $auth->createRole('author');
        $author->ruleName = $rule->name;
        $auth->add($author);
        $auth->addChild($author, $createPatient);
        $auth->addChild($author, $updatePatient);
        $auth->addChild($author, $deletePatient);
        $auth->addChild($author, $createPost);
        $auth->addChild($author, $deletePost);
        $auth->addChild($author, $viewPost);
        $auth->addChild($author, $basic);

        // добавляем роль "admin" 
        // а также все разрешения роли "basic"
        // а также все разрешения роли "author"
        $admin = $auth->createRole('admin');
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $deleteUser);
        $auth->addChild($admin, $viewUser);
        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $deletePost);
        $auth->addChild($admin, $author);
        $auth->addChild($admin, $basic);


         

        $groupRule = new UserGroupRule;
        $auth->add($groupRule);
                    
        // добавляем разрешение "updateOwnPost" и привязываем к нему правило.
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Update own post';
        $updateOwnPost->ruleName = $rule->name;
        $auth->add($updateOwnPost);

        // добавляем разрешение "deleteOwnPost" и привязываем к нему правило.
        $deleteOwnPost = $auth->createPermission('deleteOwnPost');
        $deleteOwnPost->description = 'Delete own post';
        $deleteOwnPost->ruleName = $rule->name;
        $auth->add($deleteOwnPost);

                // "updateOwnPost" будет использоваться из "updatePost"
        $auth->addChild($updateOwnPost, $updatePost);
        $auth->addChild($deleteOwnPost, $deletePost);
        
        // разрешаем "автору" обновлять его посты
        $auth->addChild($author, $updateOwnPost);
        $auth->addChild($author, $deleteOwnPost);


        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
        //$auth->assign($basic, 3);
        //$auth->assign($author, 2);
        //$auth->assign($admin, 1);
    }
}
