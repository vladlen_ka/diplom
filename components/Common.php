<?php
namespace app\components;

use Yii;
use yii\helpers\Html;


class Common 
{
        public static function transarray($array) 
        {
            foreach ($array as $key => $value) {
                $array[$key] = Yii::t('common', $value);
            }   
            
         	return $array;   
        }

        public function transelement($element) 
        {
        	return Yii::t('common', $element);
        }

}