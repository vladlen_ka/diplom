<?php
use yii\helpers\Html;
?>
<ul class="nav navbar-nav">
    <li class="lang-dropdown">
        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
        <?= Html::img('/images/'.$current->local.'.png');?>
            <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" style="min-width: 0px;">
            <?php foreach ($langs as $lang):?>
                <?php if($lang->url != 'en'){?>
                <li>
                    <?= Html::a(Html::img('/images/'.$lang->local.'.png'), '/'.$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
                </li>
                <?php }?>
            <?php endforeach;?>
        </ul>
    </li>
</ul>